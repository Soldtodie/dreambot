package Sequences;

import org.dreambot.api.methods.MethodContext;
import org.dreambot.api.methods.magic.Normal;
import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.methods.tabs.Tab;
import org.dreambot.api.methods.widget.Widget;
import org.dreambot.api.wrappers.interactive.NPC;
import org.dreambot.api.wrappers.widgets.WidgetChild;

import java.util.List;

public class MagicInstructor extends TutorialSequence {

    public MagicInstructor(MethodContext script, String guideName, Tile guidePosition) {
        super(script, guideName, guidePosition);
    }

    @Override
    public int run(int config) {

        switch(config) {

            case 610:
                getScript().getWalking().walk(new Tile(3122, 3100, 0));
                break;

            case 620:
                talkTo();
                break;

            case 630:
                if(getScript().getPlayerSettings().getConfig(1021) == 7) {
                    openTab(Tab.MAGIC);
                }
                break;

            case 640:
                talkTo();
                break;

            case 650:
                NPC chick = getScript().getNpcs().closest("Chicken");
                if(chick != null) {
                    getScript().getMagic().castSpellOn(Normal.WIND_STRIKE, chick);
                }
                break;

            case 670:
                List<WidgetChild> wg = getScript().getWidgets().getWidgetChildrenContainingText("Yes.");

                if (wg != null && !wg.isEmpty()) {
                    wg.get(0).interact();
                } else {
                    talkTo();
                }
                break;

        }

        return 150;
    }

}