package Sequences;

import CreationTool.CreationTool;
import org.dreambot.api.methods.MethodContext;
import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.methods.tabs.Tab;
import org.dreambot.api.wrappers.interactive.NPC;
import org.dreambot.api.wrappers.widgets.WidgetChild;

import java.util.List;

public class RunescapeGuide extends TutorialSequence {

    CreationTool ct;

    private String ansExpPlayer = "I am an experienced player.";
    private String ansBrandNewPlayer = "I am brand new!";
    private String ansOldPlayer = "I've played in the past";

    public RunescapeGuide(MethodContext script, String guideName, Tile guidePosition) {
        super(script, guideName, guidePosition);

        ct = new CreationTool(script.getClient().getMethodContext());
    }

    @Override
    public int run(int config) {

        switch(config) {
            case 0:
                if(ct.isOpen()) {
                    if(ct.isReady()) {
                        ct.createAccount();
                    } else {
                        ct.init();
                        return 250;
                    }
                } else {
                    List<WidgetChild> wg = getScript().getWidgets().getWidgetChildrenContainingText("To start the tutorial");

                    if(wg != null && !wg.isEmpty() && wg.get(0).isVisible()) {
                        talkTo();
                        /*
                        NPC guide = getScript().getNpcs().closest(getGuideName());
                        if (guide != null) {
                            guide.interact("Talk-to");
                        }
                        */
                    } else {

                        wg = getScript().getWidgets().getWidgetChildrenContainingText(ansOldPlayer);

                        if(wg != null && !wg.isEmpty() && wg.get(0).isVisible()) {
                            wg.get(0).interact("Continue");
                        }

                    }
                }
                break;

            case 3:
                if(getScript().getPlayerSettings().getConfig(1021) == 12) {
                    openTab(Tab.OPTIONS);
                }


                /*
                if(!getTabs().isOpen(Tab.OPTIONS)) {
                    getTabs().open(Tab.OPTIONS);
                } else {
                    clickContinue();
                }
                */
                break;

            case 7:
                List<WidgetChild> wg = getScript().getWidgets().getWidgetChildrenContainingText("On the side panel, you can now see a variety of options");

                if(wg != null && !wg.isEmpty() && wg.get(0).isVisible()) {
                    talkTo();
                    /*
                    NPC guide = getScript().getNpcs().closest(getGuideName());
                    if (guide != null) {
                        guide.interact("Talk-to");
                    }
                    */
                }

                break;
        }

        return 150;
    }


}
