package Sequences;

import org.dreambot.api.methods.MethodContext;
import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.methods.tabs.Tab;
import org.dreambot.api.wrappers.interactive.GameObject;
import org.dreambot.api.wrappers.interactive.NPC;
import org.dreambot.api.wrappers.items.Item;
import org.dreambot.api.wrappers.widgets.WidgetChild;

import java.util.List;

public abstract class TutorialSequence {

    private MethodContext script;
    private String guideName;
    private Tile guidePosition;

    public TutorialSequence(MethodContext script, String guideName, Tile guidePosition) {
        this.script = script;
        this.guideName = guideName;
        this.guidePosition = guidePosition;
    }

    public abstract int run(int config);

    public MethodContext getScript() {
        return script;
    }

    public String getGuideName() {
        return guideName;
    }

    public void talkTo() {
        NPC guide = getScript().getNpcs().closest(getGuideName());
        if (guide != null) {
            if(guide.distance() > 8 || !getScript().getMap().canReach(guide)) {
                getScript().getWalking().walk(guide);
            } else {
                guide.interact("Talk-to");
            }
        } else {
            getScript().getWalking().walk(guidePosition);
        }
    }

    public boolean openTab(Tab tab) {
        List<WidgetChild> wgs = getScript().getWidgets().getWidgets(widgetChild -> widgetChild != null && widgetChild.getActions() != null && widgetChild.getActions()[0].toLowerCase().contains(tab.name().toLowerCase()) && widgetChild.isVisible());
        if(wgs != null && !wgs.isEmpty()) {
            for(WidgetChild w : wgs) {
                w.interact();
            }
            return tab.isOpen(getScript().getClient());
        }
        return false;
    }

    public boolean pleaseWait() {
        List<WidgetChild> wg = getScript().getWidgets().getWidgetChildrenContainingText("Please wait.");
        if (wg != null && !wg.isEmpty()) {
            for(WidgetChild w : wg) {
                if(w != null && w.isVisible()) {
                    return true;
                }
            }
        }
        return false;
    }

    public void chopping() {
        if(getScript().getLocalPlayer().isStandingStill() || !getScript().getLocalPlayer().isAnimating()) {
            GameObject tree = getScript().getGameObjects().closest(g -> g != null && g.getName() != null && g.getName().equals("Tree") && getScript().getMap().canReach(g));
            if (tree != null) {
                tree.interact();
            }
        }
    }

    public void cooking() {
        if(getScript().getInventory().count("Raw shrimps") > 0) {
            if(getScript().getTabs().isOpen(Tab.INVENTORY) || getScript().getTabs().open(Tab.INVENTORY)) {

                if(getScript().getLocalPlayer().isStandingStill() || !getScript().getLocalPlayer().isAnimating()) {
                    GameObject fire = getScript().getGameObjects().closest("Fire");
                    if (fire != null) {
                        Item it = getScript().getInventory().get("Raw shrimps");
                        if (it != null) {
                            it.useOn(fire);
                        }
                    } else {
                        //Light a new fire!
                        firemaking();
                    }
                }

            }
        } else {
            fishing();
        }
    }

    public void firemaking() {
        if(getScript().getInventory().count("Logs") < 1) {
            //Chop a tree
            chopping();
        } else {
            /*
                First find position for firemaking!
             */
            if(getScript().getLocalPlayer().isStandingStill() || !getScript().getLocalPlayer().isAnimating()) {
                Item it = getScript().getInventory().get("Tinderbox");
                if (it != null) {
                    it.useOn("Logs");
                }
            }
        }
    }

    public void fishing() {
        if(getScript().getLocalPlayer().isStandingStill() || !getScript().getLocalPlayer().isAnimating()) {
            NPC spot = getScript().getNpcs().closest(npc -> npc != null && npc.getName() != null && npc.getName().equals("Fishing spot") && npc.hasAction("Net"));
            if (spot != null) {
                spot.interact("Net");
            }
        }
    }
}
