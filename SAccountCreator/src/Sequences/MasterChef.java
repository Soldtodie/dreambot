package Sequences;

import org.dreambot.api.methods.MethodContext;
import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.methods.tabs.Tab;
import org.dreambot.api.wrappers.interactive.GameObject;
import org.dreambot.api.wrappers.interactive.NPC;
import org.dreambot.api.wrappers.items.Item;

public class MasterChef extends TutorialSequence {

    public MasterChef(MethodContext script, String guideName, Tile guidePosition) {
        super(script, guideName, guidePosition);
    }

    @Override
    public int run(int config) {

        switch(config) {

            case 120:
                getScript().getWalking().walk(new Tile(3087, 3092, 0));
                break;

            case 130:
                talkTo();
                //getScript().getWalking().walk(new Tile(3076, 3084, 0));
                break;

            case 140:
                talkTo();
                break;

            case 150:
                makeDough();
                break;

            case 160:
                if(getScript().getLocalPlayer().isStandingStill() || !getScript().getLocalPlayer().isAnimating()) {
                    if(getScript().getTabs().isOpen(Tab.INVENTORY) || getScript().getTabs().open(Tab.INVENTORY)) {
                        if(getScript().getInventory().isItemSelected()) {
                            getScript().getInventory().deselect();
                        }
                        Item it = getScript().getInventory().get("Bread dough");
                        if(it != null) {
                            GameObject range = getScript().getGameObjects().closest("Range");
                            if(range != null) {
                                it.useOn(range);
                            }
                        } else {
                            makeDough();
                        }
                    }
                }
                break;

            case 170:
                if(getScript().getPlayerSettings().getConfig(1021) == 14) {
                    openTab(Tab.MUSIC);
                }
                break;

            case 180:
                getScript().getWalking().walk(new Tile(3070, 3090, 0));
                break;

            case 183:
                if(getScript().getPlayerSettings().getConfig(1021) == 13) {
                    openTab(Tab.EMOTES);
                }
                break;

            case 187:
                getScript().getEmotes().doRandomEmote();
                break;

            case 190:
                if(getScript().getPlayerSettings().getConfig(1021) == 12) {
                    openTab(Tab.OPTIONS);
                }
                break;

            case 200:
                getScript().getWalking().toggleRun();
                break;

        }

        return 150;
    }

    public void makeDough() {
        if(getScript().getTabs().isOpen(Tab.INVENTORY) || getScript().getTabs().open(Tab.INVENTORY)) {
            if(getScript().getInventory().isItemSelected()) {
                getScript().getInventory().deselect();
            }
            Item pot = getScript().getInventory().get("Pot of flour");
            Item bucket = getScript().getInventory().get("Bucket of water");
            if(pot != null && bucket != null) {
                pot.useOn(bucket);
                //Or bucket.useOn(pot);
            } else {
                //Talk-to masterChef again!
                talkTo();
            }
        }
    }

}