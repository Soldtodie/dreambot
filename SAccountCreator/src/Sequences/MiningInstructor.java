package Sequences;

import org.dreambot.api.methods.MethodContext;
import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.methods.tabs.Tab;
import org.dreambot.api.wrappers.interactive.GameObject;
import org.dreambot.api.wrappers.interactive.NPC;
import org.dreambot.api.wrappers.items.Item;
import org.dreambot.api.wrappers.widgets.WidgetChild;

import java.util.List;

public class MiningInstructor extends TutorialSequence {

    public MiningInstructor(MethodContext script, String guideName, Tile guidePosition) {
        super(script, guideName, guidePosition);
    }

    @Override
    public int run(int config) {

        switch(config) {

            case 250:
                if(getScript().getLocalPlayer().isStandingStill() || !getScript().getLocalPlayer().isAnimating()) {
                    GameObject ladder = getScript().getGameObjects().closest("Ladder");
                    if (ladder != null) {
                        ladder.interact("Climb-down");
                    }
                }
                break;

            case 260:
                talkTo();
                break;

            case 270:
                /*
                GameObject rock = getGameObjects().getTopObjectOnTile(new Tile(3077, 9504, 0));
                if(rock != null) {
                    rock.interact("Prospect");
                }
                */

                if(!pleaseWait()) {
                    GameObject rock = getScript().getGameObjects().getTopObjectOnTile(new Tile(3085, 9503, 0));
                    if (rock != null) {
                        rock.interact("Prospect");
                    }
                }
                break;

            case 281:
                //First copper!
                if(!pleaseWait()) {
                    GameObject rock = getScript().getGameObjects().getTopObjectOnTile(new Tile(3077, 9504, 0));
                    if (rock != null) {
                        rock.interact("Prospect");
                    }
                }
                break;

            case 291:
                talkTo();
                break;

            case 300:
                if(!pleaseWait()) {
                    GameObject rock = getScript().getGameObjects().getTopObjectOnTile(new Tile(3085, 9503, 0));
                    if(rock != null) {
                        rock.interact("Mine");
                    }
                }

                break;

            case 311:
                if(!pleaseWait()) {
                    GameObject rock = getScript().getGameObjects().getTopObjectOnTile(new Tile(3077, 9504, 0));
                    if(rock != null) {
                        rock.interact("Mine");
                    }
                }
                break;

            case 320:
                if(getScript().getTabs().isOpen(Tab.INVENTORY) || getScript().getTabs().open(Tab.INVENTORY)) {
                    if(getScript().getInventory().isItemSelected()) {
                        getScript().getInventory().deselect();
                    }
                    if(getScript().getLocalPlayer().isStandingStill() || !getScript().getLocalPlayer().isAnimating()) {
                        GameObject obj = getScript().getGameObjects().closest("Furnace");
                        if (obj != null) {
                            getScript().getWalking().walk(obj);
                            Item it = getScript().getInventory().get("Copper ore");
                            if (it != null) {
                                it.useOn(obj);
                            }
                        }
                    }

                }
                break;

            case 330:
                talkTo();
                break;

            case 340:
                GameObject obj = getScript().getGameObjects().closest("Anvil");
                if(obj != null) {
                    if(obj.distance() < 6 || obj.isOnScreen()) {
                        obj.interact("Smith");
                    } else {
                        getScript().getWalking().walk(obj);
                    }
                }
                break;

            case 350:
                //Check if smithing interface is open and open it

                if(getScript().getLocalPlayer().isStandingStill() || !getScript().getLocalPlayer().isAnimating()) {
                    List<WidgetChild> wg = getScript().getWidgets().getWidgets(w -> w != null && w.getActions() != null && w.getActions()[0].equals("Smith 1"));

                    if (wg != null && !wg.isEmpty()) {

                        wg.get(0).interact("Smith 1");

                    }
                }
                break;

        }

        return 150;
    }

}