package Sequences;

import org.dreambot.api.methods.MethodContext;
import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.methods.tabs.Tab;
import org.dreambot.api.wrappers.interactive.NPC;

public class SurvivalExpert extends TutorialSequence {

    public SurvivalExpert(MethodContext script, String guideName, Tile guidePosition) {
        super(script, guideName, guidePosition);
    }

    @Override
    public int run(int config) {

        switch(config) {

            case 10:
                talkTo();
                //getScript().getWalking().walk(new Tile(3102, 3107, 0));
                break;

            case 20:
                /*
                NPC guide = getScript().getNpcs().closest(getGuideName());
                if(guide != null) {
                    guide.interact("Talk-to");
                }
                */
                talkTo();
                break;

            case 30:
                if(getScript().getPlayerSettings().getConfig(1021) == 4) {
                    openTab(Tab.INVENTORY);
                }
                break;

            case 40:
                chopping();
                break;

            case 50:
                firemaking();
                break;

            case 60:
                if(getScript().getPlayerSettings().getConfig(1021) == 2) {
                    openTab(Tab.STATS);
                }
                break;

            case 70:
                /*
                guide = getScript().getNpcs().closest(getGuideName());
                if (guide != null) {
                    guide.interact("Talk-to");
                }
                */
                talkTo();
                break;

            case 80: // Fishing
                fishing();
                //Fish again or do next step?
                break;

            case 90: // Cooking
                cooking();
                break;

            case 100:
                cooking();
                break;

            case 110: //Fishing
                cooking();
                break;

        }
        return 150;
    }

}