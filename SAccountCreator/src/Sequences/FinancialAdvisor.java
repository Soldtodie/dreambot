package Sequences;

import org.dreambot.api.methods.MethodContext;
import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.wrappers.interactive.GameObject;
import org.dreambot.api.wrappers.interactive.NPC;
import org.dreambot.api.wrappers.widgets.WidgetChild;

import java.util.Arrays;
import java.util.List;

public class FinancialAdvisor extends TutorialSequence {

    public FinancialAdvisor(MethodContext script, String guideName, Tile guidePosition) {
        super(script, guideName, guidePosition);
    }

    @Override
    public int run(int config) {

        switch(config) {

            case 500:
                //After killing

                GameObject ladder = getScript().getGameObjects().closest("Ladder");
                if(ladder != null) {
                    if(ladder.isOnScreen() || ladder.distance() < 5) {
                        ladder.interact("Climb-up");
                    } else {
                        getScript().getWalking().walk(ladder);
                    }
                } else {
                    getScript().getWalking().walk(new Tile(3111, 9525, 0));
                }
                break;

            case 510:
                List<WidgetChild> wg = getScript().getWidgets().getWidgetChildrenContainingText("Yes.");

                if (wg != null && !wg.isEmpty()) {
                    wg.get(0).interact();
                } else {
                    GameObject bank = getScript().getGameObjects().closest("Bank booth");
                    if (bank != null) {
                        if (bank.isOnScreen() || bank.distance() < 5) {
                            bank.interact("Use");
                        } else {
                            getScript().getWalking().walk(new Tile(3122, 3123, 0));
                        }
                    } else {
                        getScript().getWalking().walk(new Tile(3122, 3123, 0));
                    }
                }
                break;

            case 520:
                //Doesn't work... getBank().close();

                if(getScript().getBank().isOpen()) {
                    wg = getScript().getWidgets().getWidgets(w -> w != null && w.getActions() != null && Arrays.asList(w.getActions()).contains("Close"));

                    if (wg != null && !wg.isEmpty()) {
                        for (WidgetChild w : wg) {
                            for (String action : w.getActions()) {
                                if (action != null && action.equals("Close")) {
                                    w.interact("Close");
                                }
                            }
                        }
                    }
                } else {
                    GameObject poll = getScript().getGameObjects().closest("Poll booth");
                    if (poll != null) {
                        poll.interact("Use");
                    }
                }
                break;

            case 525:
                wg = getScript().getWidgets().getWidgets(w -> w != null && w.getActions() != null && Arrays.asList(w.getActions()).contains("Close"));

                if (wg != null && !wg.isEmpty()) {
                    for (WidgetChild w : wg) {
                        for (String action : w.getActions()) {
                            if (action != null && action.equals("Close")) {
                                w.interact("Close");
                            }
                        }
                    }
                } else {
                    getScript().getWalking().walk(new Tile(3127, 3124, 0));
                }
                break;

            case 530:
                /*
                NPC guide = getScript().getNpcs().closest(getGuideName());
                if (guide != null) {
                    if(getScript().getMap().canReach(guide)) {
                        guide.interact("Talk-to");
                    } else {
                        getScript().getWalking().walk(guide);
                    }

                }
                */
                talkTo();
                break;

        }

        return 150;
    }

}