package Sequences;

import org.dreambot.api.methods.MethodContext;
import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.methods.tabs.Tab;
import org.dreambot.api.wrappers.interactive.NPC;

public class QuestGuide extends TutorialSequence {

    public QuestGuide(MethodContext script, String guideName, Tile guidePosition) {
        super(script, guideName, guidePosition);
    }

    @Override
    public int run(int config) {

        switch(config) {

            case 210:
                talkTo();
                //getScript().getWalking().walk(new Tile(3086, 3122, 0));
                break;

            case 220:
                talkTo();
                break;

            case 230:
                if(getScript().getPlayerSettings().getConfig(1021) == 3) {
                    openTab(Tab.QUEST);
                }
                break;

            case 240:
                /*
                if(getScript().getLocalPlayer().isStandingStill() || !getScript().getLocalPlayer().isAnimating()) {
                    talkTo();
                }
                */
                talkTo();
                break;

        }

        return 150;
    }

}