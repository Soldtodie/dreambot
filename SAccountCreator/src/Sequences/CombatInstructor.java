package Sequences;

import org.dreambot.api.methods.MethodContext;
import org.dreambot.api.methods.container.impl.equipment.EquipmentSlot;
import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.methods.tabs.Tab;
import org.dreambot.api.wrappers.interactive.NPC;
import org.dreambot.api.wrappers.items.Item;
import org.dreambot.api.wrappers.widgets.WidgetChild;

import java.util.List;

public class CombatInstructor extends TutorialSequence {

    public CombatInstructor(MethodContext script, String guideName, Tile guidePosition) {
        super(script, guideName, guidePosition);
    }

    @Override
    public int run(int config) {

        switch(config) {

            case 360:
                getScript().getWalking().walk(new Tile(3098, 9502, 0));
                break;

            case 370:
                talkTo();
                break;

            case 390:
                if(getScript().getPlayerSettings().getConfig(1021) == 5) {
                    openTab(Tab.EQUIPMENT);
                }
                break;

            case 400:
                List<WidgetChild> wg = getScript().getWidgets().getWidgets(w -> w != null && w.getActions() != null && w.getActions()[0].equals("View equipment stats"));

                if(wg != null && !wg.isEmpty()) {
                    wg.get(0).interact("View equipment stats");
                }
                break;

            case 405:
                Item it = getScript().getInventory().get("Bronze dagger");
                if(it != null) {
                    it.interact("Equip");
                }
                break;

            case 410:
                wg = getScript().getWidgets().getWidgets(w -> w != null && w.getActions() != null && w.getActions()[0].equals("Close"));

                if(wg != null && !wg.isEmpty()) {
                    wg.get(0).interact("Close");
                } else {
                    talkTo();
                }
                break;

            case 420:
                getScript().getEquipment().equip(EquipmentSlot.WEAPON, "Bronze sword");
                getScript().getEquipment().equip(EquipmentSlot.WEAPON, "Wooden shield");
                //getEquipment().unequip(EquipmentSlot.WEAPON);
                break;

            case 430:
                if(getScript().getPlayerSettings().getConfig(1021) == 1) {
                    openTab(Tab.COMBAT);
                }
                break;

            case 440:
                NPC rat = getScript().getNpcs().closest("Giant rat");
                if(rat != null) {
                    getScript().getWalking().walk(rat);
                }
                break;

            case 450:
                if(getScript().getLocalPlayer().isInCombat() && !getScript().getLocalPlayer().isInteractedWith()) {
                    getScript().getLocalPlayer().getCharacterInteractingWithMe().interact("Attack");
                }
                if(getScript().getLocalPlayer().isStandingStill() || !getScript().getLocalPlayer().isAnimating()) {
                    NPC ratty = getScript().getNpcs().closest("Giant rat");
                    if(ratty != null) {
                        ratty.interact("Attack");
                    }
                }
                break;

            case 470:
                talkTo();
                break;

            case 480:
                getScript().getEquipment().equip(EquipmentSlot.WEAPON, "Bronze arrow");
                getScript().getEquipment().equip(EquipmentSlot.WEAPON, "Shortbow");

                NPC ratty = getScript().getNpcs().closest("Giant rat");
                if(ratty != null) {
                    ratty.interact("Attack");
                }
                break;

            case 490:
                //After attacking before killing
                getScript().getEquipment().equip(EquipmentSlot.WEAPON, "Bronze arrow");
                getScript().getEquipment().equip(EquipmentSlot.WEAPON, "Shortbow");

                ratty = getScript().getNpcs().closest("Giant rat");
                if(ratty != null) {
                    ratty.interact("Attack");
                }
                break;


        }

        return 150;
    }

}