package Sequences;

import org.dreambot.api.methods.MethodContext;
import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.methods.tabs.Tab;

public class BrotherBrace extends TutorialSequence {

    public BrotherBrace(MethodContext script, String guideName, Tile guidePosition) {
        super(script, guideName, guidePosition);
    }

    @Override
    public int run(int config) {

        switch(config) {

            case 540:
                talkTo();
                //getScript().getWalking().walk(new Tile(3132, 3124, 0));
                break;

            case 550:
                talkTo();
                break;

            case 560:
                if(getScript().getPlayerSettings().getConfig(1021) == 6) {
                    openTab(Tab.PRAYER);
                }
                break;

            case 570:
                talkTo();
                break;

            case 580:
                if(getScript().getPlayerSettings().getConfig(1021) == 9) {
                    openTab(Tab.FRIENDS);
                }
                break;

            case 590:
                if(getScript().getPlayerSettings().getConfig(1021) == 10) {
                    openTab(Tab.IGNORE);
                }
                break;

            case 600:
                talkTo();
                break;

        }

        return 150;
    }

}
