package CreationTool;

import org.dreambot.api.methods.MethodContext;
import org.dreambot.api.methods.tabs.Tab;
import org.dreambot.api.methods.widget.Widget;
import org.dreambot.api.wrappers.widgets.WidgetChild;

import java.util.List;

import static org.dreambot.api.methods.Calculations.random;

/**
 * Created by Peter on 03.03.2017.
 */
public class CreationTool {

    MethodContext mp;

    //Change Design

    // 269 97 Message = "Use the buttons below to design your player"

    //Buttons Sprite ID 1 (enabled) 308 left ; 309 right
    //Head left 269 106 ; Head right 269 113

    //Accept 269 Tooltip = "Accept"
    //Male 269 Tooltip = "Male"
    //Female 269 Tooltip = "Female"

    //Change head
    //Recolour hair

    public CreationTool(MethodContext mp) {
        this.mp = mp;
        init();
    }

    public void createAccount() {
        if(random(0, 1) == 0) {
            wgs[24].interact();
        } else {
            wgs[25].interact();
        }
        mp.sleep(random(100, 1000));
        for(int i = 0; i < random(2, 30); i++) {
            wgs[random(0, 23)].interact();
            mp.sleep(random(50, 500));
        }
        mp.sleep(random(500, 1500));
        acceptWidget.interact();
    }

    public boolean isReady() {
        return isInitialized() && isOpen();
    }

    public void init() {
        if(!isInitialized() && isOpen()) {
            setAcceptButton();
            setGenderButtons();
            setLeftRightButtons();
        }
    }

    public boolean isInitialized() {
        return colorSkinRight != -1 && genderFemale != -1 && accept != -1;
    }

    public boolean isOpen() {
        List<WidgetChild> widgets = getRootWidgets();
        if(widgets != null) {
            for(WidgetChild wid : widgets) {
                if(wid != null && wid.getText().contains("Use the buttons below to design your player")) {
                    return true;
                }
            }
        }
        return false;
    }

    public void setAcceptButton() {
        List<WidgetChild> widgets = getRootWidgets();
        if(widgets != null) {
            for(int i = 0; i < widgets.size(); i++) {
                if(widgets.get(i) != null && widgets.get(i).getTooltip() != null && !widgets.get(i).getTooltip().equals("")) {

                    if(widgets.get(i).getTooltip().equals("Accept")) {
                        accept = i;
                        acceptWidget = widgets.get(i);
                        return;
                    }

                }
            }
        }
    }

    public void setGenderButtons() {
        List<WidgetChild> widgets = getRootWidgets();
        if(widgets != null) {
            for(int i = 0; i < widgets.size(); i++) {
                if(widgets.get(i) != null && widgets.get(i).getTooltip() != null && !widgets.get(i).getTooltip().equals("")) {

                    switch(widgets.get(i).getTooltip()) {
                        case "Male":
                            genderMale = i;
                            wgs[24] = widgets.get(i);
                        case "Female":
                            genderFemale = i;
                            wgs[25] = widgets.get(i);
                    }

                }
            }
        }
    }

    public void setLeftRightButtons() {

        List<WidgetChild> widgets = getRootWidgets();
        if(widgets != null) {
            for(int i = 0; i < widgets.size(); i++) {
                if(widgets.get(i) != null && (widgets.get(i).getTextureId() == buttonLeft || widgets.get(i).getTextureId() == buttonRight)) {

                    switch(widgets.get(i).getTooltip()) {
                        case "Change head":
                            if(widgets.get(i).getTextureId() == buttonLeft) {
                                designHeadLeft = i;
                                wgs[0] = widgets.get(i);
                            } else {
                                designHeadRight = i;
                                wgs[1] = widgets.get(i);
                            }
                        case "Change jaw":
                            if(widgets.get(i).getTextureId() == buttonLeft) {
                                designJawLeft = i;
                                wgs[2] = widgets.get(i);
                            } else {
                                designJawRight = i;
                                wgs[3] = widgets.get(i);
                            }
                        case "Change torso":
                            if(widgets.get(i).getTextureId() == buttonLeft) {
                                designTorsoLeft = i;
                                wgs[4] = widgets.get(i);
                            } else {
                                designTorsoRight = i;
                                wgs[5] = widgets.get(i);
                            }
                        case "Change arms":
                            if(widgets.get(i).getTextureId() == buttonLeft) {
                                designArmsLeft = i;
                                wgs[6] = widgets.get(i);
                            } else {
                                designArmsRight = i;
                                wgs[7] = widgets.get(i);
                            }
                        case "Change hands":
                            if(widgets.get(i).getTextureId() == buttonLeft) {
                                designHandsLeft = i;
                                wgs[8] = widgets.get(i);
                            } else {
                                designHandsRight = i;
                                wgs[9] = widgets.get(i);
                            }
                        case "Change legs":
                            if(widgets.get(i).getTextureId() == buttonLeft) {
                                designLegsLeft = i;
                                wgs[10] = widgets.get(i);
                            } else {
                                designLegsRight = i;
                                wgs[11] = widgets.get(i);
                            }
                        case "Change feet":
                            if(widgets.get(i).getTextureId() == buttonLeft) {
                                designFeetLeft = i;
                                wgs[12] = widgets.get(i);
                            } else {
                                designFeetRight = i;
                                wgs[13] = widgets.get(i);
                            }


                        case "Recolour hair":
                            if(widgets.get(i).getTextureId() == buttonLeft) {
                                colorHairLeft = i;
                                wgs[14] = widgets.get(i);
                            } else {
                                colorHairRight = i;
                                wgs[15] = widgets.get(i);
                            }
                        case "Recolour torso":
                            if(widgets.get(i).getTextureId() == buttonLeft) {
                                colorTorsoLeft = i;
                                wgs[16] = widgets.get(i);
                            } else {
                                colorTorsoRight = i;
                                wgs[17] = widgets.get(i);
                            }
                        case "Recolour legs":
                            if(widgets.get(i).getTextureId() == buttonLeft) {
                                colorLegsLeft = i;
                                wgs[18] = widgets.get(i);
                            } else {
                                colorLegsRight = i;
                                wgs[19] = widgets.get(i);
                            }
                        case "Recolour feet":
                            if(widgets.get(i).getTextureId() == buttonLeft) {
                                colorFeetLeft = i;
                                wgs[20] = widgets.get(i);
                            } else {
                                colorFeetRight = i;
                                wgs[21] = widgets.get(i);
                            }
                        case "Recolour skin":
                            if(widgets.get(i).getTextureId() == buttonLeft) {
                                colorSkinLeft = i;
                                wgs[22] = widgets.get(i);
                            } else {
                                colorSkinRight = i;
                                wgs[23] = widgets.get(i);
                            }
                    }
                }
            }
        }

    }

    public List<WidgetChild> getRootWidgets() {
        Widget wg = mp.getWidgets().getWidget(root);
        if(wg != null) {
            return wg.getChildren();
        }
        return null;
    }

    final int root = 269;
    final int buttonLeft = 308;
    final int buttonRight = 309;

    WidgetChild[] wgs = new WidgetChild[26];
    WidgetChild acceptWidget;

    int designHeadLeft = -1;
    int designJawLeft = -1;
    int designTorsoLeft = -1;
    int designArmsLeft = -1;
    int designHandsLeft = -1;
    int designLegsLeft = -1;
    int designFeetLeft = -1;

    int colorHairLeft = -1;
    int colorTorsoLeft = -1;
    int colorLegsLeft = -1;
    int colorFeetLeft = -1;
    int colorSkinLeft = -1;

    int designHeadRight = -1;
    int designJawRight = -1;
    int designTorsoRight = -1;
    int designArmsRight = -1;
    int designHandsRight = -1;
    int designLegsRight = -1;
    int designFeetRight = -1;

    int colorHairRight = -1;
    int colorTorsoRight = -1;
    int colorLegsRight = -1;
    int colorFeetRight = -1;
    int colorSkinRight = -1;

    int genderMale = -1;
    int genderFemale = -1;

    int accept = -1;

}
