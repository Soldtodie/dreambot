/**
 * Created by Peter on 7/23/2017.
 */

import Sequences.*;
import CreationTool.CreationTool;
import org.dreambot.api.methods.container.impl.equipment.EquipmentSlot;
import org.dreambot.api.methods.magic.Normal;
import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.methods.tabs.Tab;
import org.dreambot.api.script.AbstractScript;
import org.dreambot.api.script.Category;
import org.dreambot.api.script.ScriptManifest;
import org.dreambot.api.utilities.Timer;
import org.dreambot.api.wrappers.interactive.GameObject;
import org.dreambot.api.wrappers.interactive.NPC;
import org.dreambot.api.wrappers.items.Item;
import org.dreambot.api.wrappers.widgets.WidgetChild;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ScriptManifest(version = 1, name = "SAccountCreator", author = "Soldtodie", category = Category.MISC, description = "Create accounts using g2Captcha and finishing tutorial island on them.")

public class SAccountCreator extends AbstractScript {

    Timer timer = new Timer();

    int config = 281; // Progress id

    //CreationTool ct;

    /*
    String runescapeGuide = "RuneScape Guide";
    String survivalExpert = "Survival Expert";
    String masterChef = "Master Chef";
    String questGuide = "Quest Guide";
    String miningInstructor = "Mining Instructor";
    String combatInstructor = "Combat Instructor";
    String financialAdvisor = "Financial Advisor";
    String brotherBrace = "Brother Brace";
    String magicInstructor = "Magic Instructor";
    */

    /*
    String ansExpPlayer = "I am an experienced player.";
    String ansBrandNewPlayer = "I am brand new!";
    String ansOldPlayer = "I've played in the past";
    */

    /*
    RunescapeGuide runescapeGuide;
    SurvivalExpert survivalExpert;
    MasterChef masterChef;
    QuestGuide questGuide;
    MiningInstructor miningInstructor;
    CombatInstructor combatInstructor;
    FinancialAdvisor financialAdvisor;
    BrotherBrace brotherBrace;
    MagicInstructor magicInstructor;
    */

    ArrayList<TutorialSequence> sequences;

    @Override
    public void onStart() {

        sequences = new ArrayList<>();

        //ct = new CreationTool(getClient().getMethodContext());

        sequences.add(new RunescapeGuide(getClient().getMethodContext(), "RuneScape Guide", new Tile(0, 0, 0)));
        sequences.add(new SurvivalExpert(getClient().getMethodContext(), "Survival Expert", new Tile(3102, 3107, 0)));
        sequences.add(new MasterChef(getClient().getMethodContext(), "Master Chef", new Tile(3076, 3084, 0)));
        sequences.add(new QuestGuide(getClient().getMethodContext(), "Quest Guide", new Tile(3086, 3122, 0)));
        sequences.add(new MiningInstructor(getClient().getMethodContext(), "Mining Instructor", new Tile(3080, 9505, 0)));
        sequences.add(new CombatInstructor(getClient().getMethodContext(), "Combat Instructor", new Tile(3106, 9509, 0)));
        sequences.add(new FinancialAdvisor(getClient().getMethodContext(), "Financial Advisor", new Tile(3127, 3124, 0)));
        sequences.add(new BrotherBrace(getClient().getMethodContext(), "Brother Brace", new Tile(3127, 3107, 0)));
        sequences.add(new MagicInstructor(getClient().getMethodContext(), "Magic Instructor", new Tile(3141, 3087, 0)));

        /*
        runescapeGuide = new RunescapeGuide(getClient().getMethodContext(), "RuneScape Guide");
        survivalExpert = new SurvivalExpert(getClient().getMethodContext(), "Survival Expert");
        masterChef = new MasterChef(getClient().getMethodContext(), "Master Chef");
        questGuide = new QuestGuide(getClient().getMethodContext(), "Quest Guide");
        miningInstructor = new MiningInstructor(getClient().getMethodContext(), "Mining Instructor");
        combatInstructor = new CombatInstructor(getClient().getMethodContext(), "Combat Instructor");
        financialAdvisor = new FinancialAdvisor(getClient().getMethodContext(), "Financial Advisor");
        brotherBrace = new BrotherBrace(getClient().getMethodContext(), "Brother Brace");
        magicInstructor = new MagicInstructor(getClient().getMethodContext(), "Magic Instructor");
        */

        timer.reset();
    }

    public boolean doContinues() {
        List<WidgetChild> wg = getWidgets().getWidgetChildrenContainingText("Click here to continue");

        if (wg != null && !wg.isEmpty()) {
            for(WidgetChild w : wg) {
                if(w != null && w.isVisible()) {
                    w.interact("Continue");
                    return false;
                }
            }
        }

        wg = getWidgets().getWidgetChildrenContainingText("Click to continue");

        if (wg != null && !wg.isEmpty()) {
            for(WidgetChild w : wg) {
                if(w != null && w.isVisible()) {
                    w.interact();
                    return false;
                }
            }
        }

        return true;
    }

    //List<WidgetChild> wg;

    @Override
    public int onLoop() {

        if(!getClient().isLoggedIn()) {
            return 250;
        }

        if(doContinues()) {
            sequences.forEach(seq -> sleep(seq.run(getPlayerSettings().getConfig(config))));
        }

        if(getPlayerSettings().getConfig(config) == 1000) {
            log("Tutorial Island finished! Logging out!");
            if (getTabs().isOpen(Tab.LOGOUT) || getTabs().open(Tab.LOGOUT)) {
                sleep(50, 360);
                List<WidgetChild> wg = getWidgets().getWidgets(widgetChild -> widgetChild.getText() != null && widgetChild.getActions() != null && widgetChild.getActions()[0].equals("Logout"));

                if (wg != null && wg.size() > 0) {
                    wg.get(0).interact("Logout");
                    sleepUntil(() -> !getClient().isLoggedIn(), 3500);
                }

            }
        }

        return 150;

        //3115, 3118, 0

        /*
        switch(getPlayerSettings().getConfig(config)) {
            case 0:
                if(ct.isOpen()) {
                    if (ct.isReady()) {
                        ct.createAccount();
                    } else {
                        ct.init();
                        return 250;
                    }
                } else {
                    wg = getWidgets().getWidgetChildrenContainingText("To start the tutorial");

                    if (wg != null && !wg.isEmpty() && wg.get(0).isVisible()) {
                        NPC guide = getNpcs().closest(runescapeGuide);
                        if (guide != null) {
                            guide.interact("Talk-to");
                        }
                    } else {

                        wg = getWidgets().getWidgetChildrenContainingText(ansOldPlayer);

                        if (wg != null && !wg.isEmpty() && wg.get(0).isVisible()) {
                            wg.get(0).interact("Continue");
                        }

                        clickContinue();
                    }
                }
                break;

            case 3:
                if(!clickContinue()) {
                    if(getPlayerSettings().getConfig(1021) == 12) {
                        openTab(Tab.OPTIONS);
                    }
                }



                if(!getTabs().isOpen(Tab.OPTIONS)) {
                    getTabs().open(Tab.OPTIONS);
                } else {
                    clickContinue();
                }

                break;

            case 7:
                wg = getWidgets().getWidgetChildrenContainingText("On the side panel, you can now see a variety of options");

                if (wg != null && !wg.isEmpty() && wg.get(0).isVisible()) {
                    NPC guide = getNpcs().closest(runescapeGuide);
                    if (guide != null) {
                        guide.interact("Talk-to");
                    }
                }

                clickContinue();
                break;

            case 10:
                getWalking().walk(new Tile(3102, 3107, 0));
                break;

            case 20:
                if(!clickContinue()) {
                    NPC guide = getNpcs().closest(survivalExpert);
                    if (guide != null) {
                        guide.interact("Talk-to");
                    }
                }
                break;

            case 30:
                if(getPlayerSettings().getConfig(1021) == 4) {
                    openTab(Tab.INVENTORY);
                } else {
                    clickContinue();
                }
                break;

            case 40:
                chop();
                break;

            case 50:
                if(!clickContinue()) {
                    fire();
                }
                break;

            case 60:
                if(!clickContinue()) {
                    if (getPlayerSettings().getConfig(1021) == 2) {
                        openTab(Tab.STATS);
                    }
                }
                break;

            case 70:
                if(!clickContinue()) {
                    NPC guide = getNpcs().closest(survivalExpert);
                    if (guide != null) {
                        guide.interact("Talk-to");
                    }
                }
                break;

            case 80: // Fishing
                fishing();
                //Fish again or do next step?
                break;

            case 90: // Cooking
                cooking();
                break;

            case 100:
                cooking();
                break;

            case 110: //Fishing
                cooking();
                break;

            case 120:
                getWalking().walk(new Tile(3087, 3092, 0));
                break;

            case 130:
                getWalking().walk(new Tile(3076, 3084, 0));
                break;

            case 140:
                masterChef();
                break;

            case 150:
                makeDough();
                break;

            case 160:
                if(getLocalPlayer().isStandingStill() || !getLocalPlayer().isAnimating()) {
                    if(getTabs().isOpen(Tab.INVENTORY) || getTabs().open(Tab.INVENTORY)) {
                        if(getInventory().isItemSelected()) {
                            getInventory().deselect();
                        }
                        Item it = getInventory().get("Bread dough");
                        if (it != null) {
                            GameObject range = getGameObjects().closest("Range");
                            if (range != null) {
                                it.useOn(range);
                            }
                        } else {
                            makeDough();
                        }
                    }
                }
                break;

            case 170:
                if(!clickContinue()) {
                    if (getPlayerSettings().getConfig(1021) == 14) {
                        openTab(Tab.MUSIC);
                    }
                }
                break;

            case 180:
                getWalking().walk(new Tile(3070, 3090, 0));
                break;

            case 183:
                if(!clickContinue()) {
                    if(getPlayerSettings().getConfig(1021) == 13) {
                        openTab(Tab.EMOTES);
                    }
                }
                break;

            case 187:
                getEmotes().doRandomEmote();
                break;

            case 190:
                if(!clickContinue()) {
                    if (getPlayerSettings().getConfig(1021) == 12) {
                        openTab(Tab.OPTIONS);
                    }
                }
                break;

            case 200:
                getWalking().toggleRun();
                break;

            case 210:
                getWalking().walk(new Tile(3086, 3122, 0));
                break;

            case 220:
                if(!clickContinue()) {
                    NPC guide = getNpcs().closest(questGuide);
                    if (guide != null) {
                        guide.interact("Talk-to");
                    }
                }
                break;

            case 230:
                if(!clickContinue()) {
                    if (getPlayerSettings().getConfig(1021) == 3) {
                        openTab(Tab.QUEST);
                    }
                }
                break;

            case 240:
                if(!clickContinue()) {
                    if(getLocalPlayer().isStandingStill() || !getLocalPlayer().isAnimating()) {
                        NPC guide = getNpcs().closest(questGuide);
                        if (guide != null) {
                            guide.interact("Talk-to");
                        }
                    }
                }
                break;

            case 250:
                if(getLocalPlayer().isStandingStill() || !getLocalPlayer().isAnimating()) {
                    GameObject ladder = getGameObjects().closest("Ladder");
                    if (ladder != null) {
                        ladder.interact("Climb-down");
                    }
                }
                break;

            case 260:
                miningInstructor();
                break;

            case 270:

                GameObject rock = getGameObjects().getTopObjectOnTile(new Tile(3077, 9504, 0));
                if(rock != null) {
                    rock.interact("Prospect");
                }


                if(!pleaseWait()) {
                    rock = getGameObjects().getTopObjectOnTile(new Tile(3085, 9503, 0));
                    if (rock != null) {
                        rock.interact("Prospect");
                    }
                }
                break;

            case 281:
                //First copper!
                if(!pleaseWait()) {
                    rock = getGameObjects().getTopObjectOnTile(new Tile(3077, 9504, 0));
                    if (rock != null) {
                        rock.interact("Prospect");
                    }
                }
                break;

            case 291:
                miningInstructor();
                break;

            case 300:
                if(!pleaseWait()) {
                    rock = getGameObjects().getTopObjectOnTile(new Tile(3085, 9503, 0));
                    if(rock != null) {
                        rock.interact("Mine");
                    }
                }

                break;

            case 311:
                if(!pleaseWait()) {
                    rock = getGameObjects().getTopObjectOnTile(new Tile(3077, 9504, 0));
                    if (rock != null) {
                        rock.interact("Mine");
                    }
                }
                break;

            case 320:
                if(getTabs().isOpen(Tab.INVENTORY) || getTabs().open(Tab.INVENTORY)) {
                    if (getInventory().isItemSelected()) {
                        getInventory().deselect();
                    }
                    if(getLocalPlayer().isStandingStill() || !getLocalPlayer().isAnimating()) {
                        GameObject obj = getGameObjects().closest("Furnace");
                        if (obj != null) {
                            getWalking().walk(obj);
                            Item it = getInventory().get("Copper ore");
                            if (it != null) {
                                it.useOn(obj);
                            }
                        }
                    }

                }
                break;

            case 330:
                if(!clickToContinue()) {
                    miningInstructor();
                }
                break;

            case 340:
                GameObject obj = getGameObjects().closest("Anvil");
                if(obj != null) {
                    if(obj.distance() < 6 || obj.isOnScreen()) {
                        obj.interact("Smith");
                    } else {
                        getWalking().walk(obj);
                    }
                }
                break;

            case 350:
                //Check if smithing interface is open and open it


                if(getLocalPlayer().isStandingStill() || !getLocalPlayer().isAnimating()) {
                    wg = getWidgets().getWidgets(w -> w != null && w.getActions() != null && w.getActions()[0].equals("Smith 1"));

                    if (wg != null && !wg.isEmpty()) {

                        wg.get(0).interact("Smith 1");

                    }
                }
                break;

            case 360:
                getWalking().walk(new Tile(3098, 9502, 0));
                break;

            case 370:
                combatInstructor();
                break;

            case 390:
                if(!clickContinue()) {
                    if (getPlayerSettings().getConfig(1021) == 5) {
                        openTab(Tab.EQUIPMENT);
                    }
                }
                break;

            case 400:
                wg = getWidgets().getWidgets(w -> w != null && w.getActions() != null && w.getActions()[0].equals("View equipment stats"));

                if (wg != null && !wg.isEmpty()) {
                    wg.get(0).interact("View equipment stats");
                }
                break;

            case 405:
                Item it = getInventory().get("Bronze dagger");
                if(it != null) {
                    it.interact("Equip");
                }
                break;

            case 410:
                wg = getWidgets().getWidgets(w -> w != null && w.getActions() != null && w.getActions()[0].equals("Close"));

                if (wg != null && !wg.isEmpty()) {
                    wg.get(0).interact("Close");
                } else {
                    combatInstructor();
                }
                break;

            case 420:
                getEquipment().equip(EquipmentSlot.WEAPON, "Bronze sword");
                getEquipment().equip(EquipmentSlot.WEAPON, "Wooden shield");
                //getEquipment().unequip(EquipmentSlot.WEAPON);
                break;

            case 430:
                if(!clickContinue()) {
                    if (getPlayerSettings().getConfig(1021) == 1) {
                        openTab(Tab.COMBAT);
                    }
                }
                break;

            case 440:
                NPC rat = getNpcs().closest("Giant rat");
                if(rat != null) {
                    getWalking().walk(rat);
                }
                break;

            case 450:
                if(getLocalPlayer().isInCombat() && !getLocalPlayer().isInteractedWith()) {
                    getLocalPlayer().getCharacterInteractingWithMe().interact("Attack");
                }
                if(getLocalPlayer().isStandingStill() || !getLocalPlayer().isAnimating()) {
                    NPC ratty = getNpcs().closest("Giant rat");
                    if (ratty != null) {
                        ratty.interact("Attack");
                    }
                }
                break;

            case 470:
                combatInstructor();
                break;

            case 480:
                if(!clickContinue()) {
                    getEquipment().equip(EquipmentSlot.WEAPON, "Bronze arrow");
                    getEquipment().equip(EquipmentSlot.WEAPON, "Shortbow");

                    NPC ratty = getNpcs().closest("Giant rat");
                    if (ratty != null) {
                        ratty.interact("Attack");
                    }

                }
                break;

            case 490:
                //After attacking before killing
                break;


            case 500:
                //After killing

                GameObject ladder = getGameObjects().closest("Ladder");
                if(ladder != null) {
                    if(ladder.isOnScreen() || ladder.distance() < 5) {
                        ladder.interact("Climb-up");
                    } else {
                        getWalking().walk(ladder);
                    }
                } else {
                    getWalking().walk(new Tile(3111, 9525, 0));
                }
                break;

            case 510:
                if(!clickContinue()) {
                    wg = getWidgets().getWidgetChildrenContainingText("Yes.");

                    if (wg != null && !wg.isEmpty()) {
                        wg.get(0).interact();
                    } else {
                        GameObject bank = getGameObjects().closest("Bank booth");
                        if (bank != null) {
                            if (bank.isOnScreen() || bank.distance() < 5) {
                                bank.interact("Use");
                            } else {
                                getWalking().walk(new Tile(3122, 3123, 0));
                            }
                        } else {
                            getWalking().walk(new Tile(3122, 3123, 0));
                        }
                    }
                }
                break;

            case 520:
                //Doesn't work... getBank().close();

                if(getBank().isOpen()) {
                    wg = getWidgets().getWidgets(w -> w != null && w.getActions() != null && Arrays.asList(w.getActions()).contains("Close"));

                    if (wg != null && !wg.isEmpty()) {
                        for (WidgetChild w : wg) {
                            for (String action : w.getActions()) {
                                if (action != null && action.equals("Close")) {
                                    w.interact("Close");
                                }
                            }
                        }
                    }
                } else {
                    if(!clickContinue()) {
                        GameObject poll = getGameObjects().closest("Poll booth");
                        if (poll != null) {
                            poll.interact("Use");
                        }
                    }
                }
                break;

            case 525:
                wg = getWidgets().getWidgets(w -> w != null && w.getActions() != null && Arrays.asList(w.getActions()).contains("Close"));

                if (wg != null && !wg.isEmpty()) {
                    for (WidgetChild w : wg) {
                        for (String action : w.getActions()) {
                            if (action != null && action.equals("Close")) {
                                w.interact("Close");
                            }
                        }
                    }
                } else {
                    getWalking().walk(new Tile(3127, 3124, 0));
                }
                break;

            case 530:
                if(!clickContinue()) {
                    NPC guide = getNpcs().closest(financialAdvisor);
                    if (guide != null) {
                        if(getMap().canReach(guide)) {
                            guide.interact("Talk-to");
                        } else {
                            getWalking().walk(guide);
                        }

                    }
                }
                break;

            case 540:
                getWalking().walk(new Tile(3132, 3124, 0));
                break;

            case 550:
                brotherBrace();
                break;

            case 560:
                if(!clickContinue()) {
                    if (getPlayerSettings().getConfig(1021) == 6) {
                        openTab(Tab.PRAYER);
                    }
                }
                break;

            case 570:
                brotherBrace();
                break;

            case 580:
                if(!clickContinue()) {
                    if (getPlayerSettings().getConfig(1021) == 9) {
                        openTab(Tab.FRIENDS);
                    }
                }
                break;

            case 590:
                if(!clickContinue()) {
                    if (getPlayerSettings().getConfig(1021) == 10) {
                        openTab(Tab.IGNORE);
                    }
                }
                break;

            case 600:
                brotherBrace();
                break;

            case 610:
                getWalking().walk(new Tile(3122, 3100, 0));
                break;

            case 620:
                magicInstructor();
                break;

            case 630:
                if(!clickContinue()) {
                    if (getPlayerSettings().getConfig(1021) == 7) {
                        openTab(Tab.MAGIC);
                    }
                }
                break;

            case 640:
                magicInstructor();
                break;

            case 650:
                if(!clickContinue()) {
                    NPC chick = getNpcs().closest("Chicken");
                    if(chick != null) {
                        getMagic().castSpellOn(Normal.WIND_STRIKE, chick);
                    }
                }
                break;

            case 670:
                wg = getWidgets().getWidgetChildrenContainingText("Yes.");

                if (wg != null && !wg.isEmpty()) {
                    wg.get(0).interact();
                } else {
                    magicInstructor();
                }
                break;

            case 1000:
                log("Tutorial Island finished! Logging out!");
                if(getTabs().isOpen(Tab.LOGOUT) || getTabs().open(Tab.LOGOUT)) {
                    sleep(50, 360);
                    List<WidgetChild> wg = getWidgets().getWidgets(widgetChild -> widgetChild.getText() != null && widgetChild.getActions() != null && widgetChild.getActions()[0].equals("Logout"));

                    if(wg != null && wg.size() > 0) {
                        wg.get(0).interact("Logout");
                        sleepUntil(() -> !getClient().isLoggedIn(), 3500);
                    }

                }
                break;

        }

        */

    }

    /*
    GameObject rock;

    public boolean pleaseWait() {
        List<WidgetChild> wg = getWidgets().getWidgetChildrenContainingText("Please wait.");
        if (wg != null && !wg.isEmpty()) {
            for(WidgetChild w : wg) {
                if(w != null && w.isVisible()) {
                    return true;
                }
            }
        }
        return false;
    }

    public void magicInstructor() {
        if(!clickContinue()) {
            NPC guide = getNpcs().closest(magicInstructor);
            if (guide != null) {
                if(guide.distance() > 6) {
                    getWalking().walk(guide);
                } else {
                    guide.interact("Talk-to");
                }
            } else {
                getWalking().walk(new Tile(3141, 3087, 0));
            }
        }
    }

    public void brotherBrace() {
        if(!clickContinue()) {
            NPC guide = getNpcs().closest(brotherBrace);
            if (guide != null) {
                if(guide.distance() > 6) {
                    getWalking().walk(guide);
                } else {
                    guide.interact("Talk-to");
                }
            } else {
                getWalking().walk(new Tile(3127, 3107, 0));
            }
        }
    }

    public void combatInstructor() {
        if(!clickContinue()) {
            NPC guide = getNpcs().closest(combatInstructor);
            if (guide != null) {
                if(guide.distance() > 8) {
                    getWalking().walk(guide);
                } else {
                    guide.interact("Talk-to");
                }
            } else {
                getWalking().walk(new Tile(3106, 9509, 0));
            }
        }
    }

    public void miningInstructor() {
        if(!clickContinue()) {
            NPC guide = getNpcs().closest(miningInstructor);
            if (guide != null) {
                if(guide.distance() > 8) {
                    getWalking().walk(guide);
                } else {
                    guide.interact("Talk-to");
                }
            } else {
                getWalking().walk(new Tile(3080, 9505, 0));
            }
        }
    }

    public void makeDough() {
        if(getTabs().isOpen(Tab.INVENTORY) || getTabs().open(Tab.INVENTORY)) {
            if(getInventory().isItemSelected()) {
                getInventory().deselect();
            }
            Item pot = getInventory().get("Pot of flour");
            Item bucket = getInventory().get("Bucket of water");
            if(pot != null && bucket != null) {
                pot.useOn(bucket);
                //Or bucket.useOn(pot);
            } else {
                //Talk-to masterChef again!
                masterChef();
            }
        }
    }

    public void masterChef() {
        if(!clickContinue()) {
            NPC guide = getNpcs().closest(masterChef);
            if (guide != null) {
                guide.interact("Talk-to");
            }
        }
    }

    public void chop() {
        if(getLocalPlayer().isStandingStill() || !getLocalPlayer().isAnimating()) {
            GameObject tree = getGameObjects().closest(g -> g != null && g.getName() != null && g.getName().equals("Tree") && getMap().canReach(g));
            if (tree != null) {
                tree.interact();
            }
        }
    }

    public void cooking() {
        if(!clickContinue()) {
            if(getInventory().count("Raw shrimps") > 0) {
                if(getTabs().isOpen(Tab.INVENTORY) || getTabs().open(Tab.INVENTORY)) {

                    if(getLocalPlayer().isStandingStill() || !getLocalPlayer().isAnimating()) {
                        GameObject fire = getGameObjects().closest("Fire");
                        if (fire != null) {
                            Item it = getInventory().get("Raw shrimps");
                            if (it != null) {
                                it.useOn(fire);
                            }
                        } else {
                            //Light a new fire!
                            fire();
                        }
                    }

                }
            } else {
                fishing();
            }
        }
    }

    public void fire() {
        if(getInventory().count("Logs") < 1) {
            //Chop a tree
            chop();
        } else {
            /*
                First find position for firemaking!

            if(getLocalPlayer().isStandingStill() || !getLocalPlayer().isAnimating()) {
                Item it = getInventory().get("Tinderbox");
                if (it != null) {
                    it.useOn("Logs");
                }
            }
        }
    }

    public void fishing() {
        if(!clickContinue()) {
            if(getLocalPlayer().isStandingStill() || !getLocalPlayer().isAnimating()) {
                NPC spot = getNpcs().closest(npc -> npc != null && npc.getName() != null && npc.getName().equals("Fishing spot") && npc.hasAction("Net"));
                if (spot != null) {
                    spot.interact("Net");
                }
            }
        }
    }

    public boolean openTab(Tab tab) {
        List<WidgetChild> wgs = getWidgets().getWidgets(widgetChild -> widgetChild != null && widgetChild.getActions() != null && widgetChild.getActions()[0].toLowerCase().contains(tab.name().toLowerCase()) && widgetChild.isVisible());
        if(wgs != null && !wgs.isEmpty()) {
            for(WidgetChild w : wgs) {
                w.interact();
            }
            return tab.isOpen(getClient());
        }
        return false;
    }

    */

    @Override
    public void onPaint(Graphics g) {
        g.setColor(Color.YELLOW);
        g.drawString("" + timer.formatTime(), 10, 60);
        g.drawString("Config(281): " + getPlayerSettings().getConfig(config), 10, 75);
        g.drawString("Config(1021): " + getPlayerSettings().getConfig(1021), 10, 90);
    }

    @Override
    public void onExit() {

    }

}
