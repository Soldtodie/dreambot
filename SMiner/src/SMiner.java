/**
 * Created by Peter on 6/20/2017.
 */

import API.API;
import API.Rock;
import API.PopUp;
import API.RemoveButton;
import API.Utility;
import API.DropStyle;
import API.Gui;
import API.Debug;
import API.UpdateThread;
import org.dreambot.api.methods.skills.Skill;
import org.dreambot.api.methods.tabs.Tab;
import org.dreambot.api.script.AbstractScript;
import org.dreambot.api.script.Category;
import org.dreambot.api.script.ScriptManifest;
import org.dreambot.api.script.listener.AdvancedMessageListener;
import org.dreambot.api.wrappers.widgets.message.Message;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;

@ScriptManifest(version = 1, author = "Soldtodie", category = Category.MINING, description = "Miner", image = "", name = "SMiner")

public class SMiner extends AbstractScript implements MouseListener {

    final Color color1 = new Color(255, 255, 255);
    final Font fontImpress = new Font("Arial", 0, 9);
    final Font fontPaint = new Font("Arial", 0, 10);
    BufferedImage background;

    String location = "";
    String dropMethod = "";
    String status = "";

    boolean selectionMode = true;

    boolean paint = true;

    long startTime;
    int startXp;
    int miningLevel;

    int gemsMined;
    int oresMined;

    String[] notDropItems = {"pickaxe", "handle", "waterskin", "coins", "flask"};

    API doc;

    JFrame gui;

    public API getDoc() {
        return doc;
    }

    Thread updateThread;

    @Override
    public void onStart() {
        doc = new API(getClient().getMethodContext());

        gui = new Gui(getDoc());

        updateThread = new Thread(new UpdateThread(getDoc()));

        startRot = getLocalPlayer().getRotation();

        RemoveButton.setImg_standard("http://i.imgur.com/TNwRwHy.png");

        //RemoveButton.setImg_hover("https://i.imgur.com/l3uMgII.png");

        background = Utility.getImage("https://i.imgur.com/Hnug8A1.png");
        startTime = System.currentTimeMillis();
        startXp = getSkills().getExperience(Skill.MINING);
        miningLevel = getSkills().getRealLevel(Skill.MINING);

        //getClient().getInstance().setDrawMouse(false);



        getClient().getInstance().addEventListener(new AdvancedMessageListener() {
            @Override
            public void onAutoMessage(Message message) {

            }

            @Override
            public void onPrivateInfoMessage(Message message) {

            }

            @Override
            public void onClanMessage(Message message) {

            }

            @Override
            public void onMessage(Message message) {

            }

            @Override
            public void onGameMessage(Message message) {
                String mess = message.getMessage().toLowerCase();
                if(mess.contains("you manage to mine") || mess.contains("you manage to quarry")) {
                    getDoc().addPopUp(new PopUp("+1", Color.WHITE, getLocalPlayer().getCenterPoint()));
                    oresMined++;
                } else if(mess.contains("just found")) {
                    gemsMined++;
                }
            }

            @Override
            public void onPlayerMessage(Message message) {

            }

            @Override
            public void onTradeMessage(Message message) {

            }

            @Override
            public void onPrivateInMessage(Message message) {

            }

            @Override
            public void onPrivateOutMessage(Message message) {

            }
        });

        updateThread.start();

    }

    @Override
    public int onLoop() {

        if(gui.isVisible()) {
            return 500;
        }

        if(selectionMode && !gui.isVisible()) {
            selectionMode = false;
        }

        if(getClient().isLoggedIn()) {

            if(getDoc().getRocks().size() < 1) {
                //Other world!
                for(Rock rock : getDoc().getStandardRocks()) {
                    getDoc().addRock(rock);
                }
            }

            if(getInventory().isFull()) {
                if(!Tab.INVENTORY.isOpen(getClient())) {
                    getTabs().open(Tab.INVENTORY);
                    sleep(50, 250);
                }
                getDoc().getInventory().dropAllExcept(DropStyle.NORMAL_DROP, notDropItems);
            } else {
                mine();
            }

        }

        return 150;
    }



    public void mine() {

        if(getDoc().getCurrentRock() == null) {

            getDoc().setCurrentRock(getDoc().getNextRock(false));
            getDoc().setHoverRock(null);

            if(getDoc().getCurrentRock() != null) {

                getDoc().getCurrentRock().interact();

            }

        } else {

            if(getDoc().getCurrentRock().isMined()) {
                getDoc().setCurrentRock(null);
            } else {

                //Create own method for this.
                if(sleepUntil(() -> (getLocalPlayer().isMoving() || getLocalPlayer().isAnimating()) && !getDoc().getCurrentRock().isMined(), 1500)) {
                    //Hover!

                    if(getDoc().getHoverRock() != null) {
                        if(!getDoc().getHoverRock().isHovering()) {
                            getDoc().getHoverRock().hover();
                        } else {
                            //Antiban // Random movement on rock!
                        }
                    } else {
                        getDoc().setHoverRock(getDoc().getNextRock(true));
                    }


                } else  {
                    getDoc().setCurrentRock(null);
                }
            }

        }



    }

    int startRot = 1536;
    long time = -1;

    @Override
    public void onPaint(Graphics g) {
        Graphics2D graphics = (Graphics2D) g;

        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        getDoc().getPopUps().forEach(e -> e.show(graphics));

        if(paint) {
            long runTime = System.currentTimeMillis() - startTime;
            int xpGained = (getSkills().getExperience(Skill.MINING) - startXp);
            int xpH = (int) ((double) xpGained / runTime * 3600000);
            int mined = (int) ((double) oresMined / runTime * 3600000);
            g.drawImage(background, 0, 1, null);
            g.setFont(fontImpress);
            g.setColor(Color.GRAY);
            g.drawString("By Soldtodie", 160, 333);
            g.setColor(color1);
            g.setFont(fontPaint);
            g.drawString("Version: " + getVersion(), 259, 257);
            g.drawString("Run Time: " + Utility.getElapsedTimeHoursMinutesSecondsString(runTime), 259, 271);
            g.drawString("Location: " + location, 259, 286);
            g.drawString("Ores mined: " + oresMined, 259, 301);
            g.drawString("Ores/Hour: " + mined, 259, 316);
            g.drawString("XP Gained: " + xpGained, 259, 331);
            g.drawString("XP/Hour: " + xpH, 386, 257);
            g.drawString("XP To Level: " + getSkills().getExperienceToLevel(Skill.MINING), 386, 271);
            g.drawString("Level: " + getSkills().getRealLevel(Skill.MINING) + " (" + (getSkills().getRealLevel(Skill.MINING) - miningLevel) + " gained)", 386, 286);
            g.drawString("Gems mined: " + gemsMined, 386, 301);
            g.drawString("Drop: " + dropMethod, 386, 316);
            g.drawString("Status: " + status, 386, 331);
        }

        getDoc().getRocks().forEach(e -> e.draw(graphics, Color.YELLOW, Color.RED, selectionMode));

        getDoc().getRocks().forEach(e -> Debug.draw(graphics, e.getButtonPosition(), Color.BLACK, e.getName(), "SpawnTime: " + e.getCalculatedSpawnTime(),  "TimeTillRespawn: " + e.getTimeTillRespawn(), "Costs: " + e.getCosts()));

    }

    @Override
    public void onExit() {
        getClient().getInstance().setDrawMouse(true);
        gui.setVisible(false);
        gui.dispose();
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {

    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

        if(getClient().isLoggedIn() && !getClient().getInstance().isMouseInputEnabled() && selectionMode) {

            getMouse().hop(mouseEvent.getPoint());
            Rock rock = Rock.getRockUnderMouse(getDoc());
            if (rock != null && !getDoc().getRocks().contains(rock)) {
                getDoc().addPopUp(new PopUp(rock.getName() + " added!", Color.GREEN, rock.getObject().getCenterPoint()));
                getDoc().addRock(rock);
                return;
            }

            for (int i = 0; i < getDoc().getRocks().size(); i++) {
                if (RemoveButton.isOver(getDoc().getRocks().get(i).getButtonPosition(), mouseEvent.getPoint())) {
                    getDoc().addPopUp(new PopUp(getDoc().getRocks().get(i).getName() + " removed!", Color.RED, getDoc().getRocks().get(i).getObject().getCenterPoint()));
                    getDoc().removeRock(i);
                }
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
