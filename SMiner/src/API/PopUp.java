package API;

import java.awt.*;
import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by Peter on 6/22/2017.
 */
public class PopUp {
    private static ArrayList<PopUp> popUps = new ArrayList<>();

    private String text;
    private Color color;
    private long startTime;
    private int showTime = 1600;
    private int waitTime = 100;
    private int upValue = 75;
    private Point pos;
    private Font font = new Font ("Monospaced", Font.BOLD | Font.ITALIC, 20);

    public PopUp(String text, Color color, Point pos) {
        this.text = text;
        this.color = color;
        this.startTime = System.currentTimeMillis();
        this.pos = pos;
    }

    public static ArrayList<PopUp> getPopUps() {

        for(int i = 0; i < popUps.size(); i++) {
            if(popUps.get(i).getShowedTime() > popUps.get(i).showTime + popUps.get(i).waitTime) {
                popUps.remove(i);
            }
        }

        return popUps;
    }

    public static void addPopUp(PopUp pop) {
        popUps.add(pop);
    }

    public long getShowedTime() {
        return System.currentTimeMillis() - this.startTime;
    }

    public void show(Graphics2D g) {
        if (g != null) {
            long showedTime = getShowedTime();
            if(showedTime <= this.showTime + this.waitTime) {
                int alpha = 255;
                int y = pos.y;
                if(showedTime >= this.waitTime) {
                    showedTime -= this.waitTime;
                    alpha = (int) (255 - (showedTime / ((double) this.showTime / 255)));
                    y = (int) (pos.y - (showedTime / ((double) this.showTime / this.upValue)));
                }
                g.setColor(new Color(this.color.getRed(), this.color.getGreen(), this.color.getBlue(), alpha));
                g.setFont(this.font);
                g.drawString(this.text, this.pos.x, y);
            }
        }
    }
}
