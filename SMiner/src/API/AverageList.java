package API;

import java.util.ArrayList;

/**
 * Created by Peter on 6/21/2017.
 */
public class AverageList {

    private ArrayList<Long> list = new ArrayList<>();
    private int size;
    private long average;

    public AverageList(int size) {
        this.size = size;
    }

    public void add(long value) {

        if(list.size() == size) {
            list.remove(0);
        }
        list.add(value);

        average = calculateAverage();

    }

    private long calculateAverage() {

        return (long)list.stream().mapToLong(l -> l).average().getAsDouble();

    }

    public long getAverage() {
        return this.average;
    }

}
