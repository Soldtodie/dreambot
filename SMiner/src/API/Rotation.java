package API;

import org.dreambot.api.methods.map.Tile;

/**
 * Created by Peter on 6/21/2017.
 */
public enum Rotation {

    NORTH(1024, new Tile(0, 1)),
    EAST(1536, new Tile(1, 0)),
    SOUTH(0, new Tile(0, -1)),
    WEST(512, new Tile(-1, 0)),
    NORTH_EAST(1280, new Tile(1, 1)),
    SOUTH_EAST(1792, new Tile(1, -1)),
    SOUTH_WEST(256, new Tile(-1, -1)),
    NORTH_WEST(768, new Tile(-1, 1));

    private int rotation;
    private Tile direction;

    Rotation(int rotation, Tile direction) {
        this.rotation = rotation;
        this.direction = direction;
    }

    public int getRotation() {
        return this.rotation;
    }

    public Tile getDirection() {
        return this.direction;
    }

}
