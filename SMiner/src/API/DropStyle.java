package API;

/**
 * Created by Peter on 01.08.2015.
 */

public enum DropStyle {

    NORMAL_DROP(new int[]{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27}),
    NORMAL_REVERT_DROP(new int[]{27,26,25,24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0}),
    SMOOTH_DROP(new int[]{0,1,2,3,7,6,5,4,8,9,10,11,15,14,13,12,16,17,18,19,23,22,21,20,24,25,26,27}),
    SMOOTH_REVERT_DROP(new int[]{27,26,25,24,20,21,22,23,19,18,17,16,12,13,14,15,11,10,9,8,4,5,6,7,3,2,1,0}),
    POWER_DROP(new int[]{0,4,8,12,16,20,24,1,5,9,13,17,21,25,2,6,10,14,18,22,26,3,7,11,15,19,23,27}),
    POWER_SMOOTH_DROP(new int[]{0,4,8,12,16,20,24,25,21,17,13,9,5,1,2,6,10,14,18,22,26,27,23,19,15,11,7,3}),
    POWER_REVERT_DROP(new int[]{3,7,11,15,19,23,27,2,6,10,14,18,22,26,1,5,9,13,17,21,25,0,4,8,12,16,20,24}),
    POWER_SMOOTH_REVERT_DROP(new int[]{3,7,11,15,19,23,27,26,22,18,14,10,6,2,1,5,9,13,17,21,25,24,20,16,12,8,4,0});
    //PYRAMID_DROP(new int[]{0,1,2,3,7,11,15,19,23,27,26,25,24,20,16,12,8,4,5,6,10,14,18,22,21,17,13,9});

    private int[] list;

    DropStyle(int[] list) {
        this.list = list;
    }

    public int[] get() {
        return this.list;
    }

}
