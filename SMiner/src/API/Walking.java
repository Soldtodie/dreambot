package API;

import org.dreambot.api.methods.map.Tile;

/**
 * Created by Peter on 7/11/2017.
 */
public class Walking extends APIScript {

    public Walking(API script) {
        super(script);
    }

    public boolean walk(Tile pos) {
        return getScript().getWalking().walk(pos);
    }

}
