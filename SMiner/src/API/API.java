package API;

import org.dreambot.api.methods.MethodContext;
import org.dreambot.api.wrappers.interactive.GameObject;
import org.dreambot.api.wrappers.items.Item;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

/**
 * Created by Peter on 6/18/2017.
 */

public class API {

    private MethodContext script;
    private Inventory inventory;
    private Utility utility;
    private Walking walking;

    public API(MethodContext script) {
        this.script = script;
        this.inventory = new Inventory(this);
        this.utility = new Utility(this);
        this.walking = new Walking(this);
    }

    public MethodContext getScript() {
        return this.script;
    }

    public Inventory getInventory() {
        return this.inventory;
    }

    public Utility getUtility() {
        return this.utility;
    }

    public Walking getWalking() {
        return this.walking;
    }

    public static int random(int minimum, int maximum) {
        Random rand = new Random();
        return (minimum + rand.nextInt((maximum - minimum) + 1));
    }

    public ArrayList<Rock> getRocks() {
        return RockWorld.getRocks(getScript().getClient().getCurrentWorld());
    }

    public ArrayList<Rock> getStandardRocks() {
        return RockWorld.getRockWorlds().get(0).getRocks();
    }

    public void deleteWorlds() {
        RockWorld.getRockWorlds().clear();
        RockWorld.addWorld(getScript().getClient().getCurrentWorld());
    }

    public void addRock(Rock rock) {
        RockWorld.addRockToWorld(this, getScript().getClient().getCurrentWorld(), rock);
    }

    public void removeRock(int index) {
        getRocks().remove(index);
    }

    public ArrayList<PopUp> getPopUps() {
        return PopUp.getPopUps();
    }

    public void addPopUp(PopUp pop) {
        PopUp.addPopUp(pop);
    }

    public void setCurrentRock(Rock rock) {
        Rock.setCurrentRock(rock);
        //setHoverRock(null);
    }

    public Rock getCurrentRock() {
        return Rock.getCurrentRock();
    }

    public void setHoverRock(Rock rock) {
        Rock.setHoverRock(rock);
    }

    public Rock getHoverRock() {
        return Rock.getHoverRock();
    }

    public Rock getNextHoverRock() {

        for(Rock rock : getRocks()) {
            if(!getCurrentRock().equals(rock)) {
                if (!rock.isMined()) {
                    return rock;
                }
            }

        }

        return null;
    }

    public Rock getNextRock(boolean next) {

        if(getRocks().size() == 0) return null;

        if(getHoverRock() != null) {
            return getHoverRock();
        }

        Rock returnRock = null;

        for(Rock rock : getRocks()) {

            if(next && getCurrentRock() != null && rock.equals(getCurrentRock())) {
                continue;
            }

            rock.update();
            rock.updateCosts(getScript().getLocalPlayer());

            if(returnRock == null || rock.getCosts() < returnRock.getCosts()) {
                returnRock = rock;
            }

        }

        return returnRock;
    }

    public String getItemName(int id) {
        Item it = new Item(id, 0, getScript().getClient().getInstance());
        if(it != null) {
            return it.getName();
        }
        return null;
    }

}

