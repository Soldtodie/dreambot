package API;

/**
 * Created by Peter on 6/25/2017.
 */
public class Statistic {

    private String text = "";
    private int calculations = -1;
    private String stuffBehind = "";
    private Statistic statBehind;
    private int distanceBetween;

    public Statistic(String text) {
        this.text = text;
    }

    public Statistic(String text, String stuffBehind) {
        this.text = text;
        this.stuffBehind = stuffBehind;
    }

    public void changeCalculations(int calculations) {
        this.calculations = calculations;
    }

    public String getText() {
        return text + ": " + calculations + "" + stuffBehind;
    }

    public Statistic addBehind(String text) {
        statBehind = new Statistic(text);
        return statBehind;
    }

}
