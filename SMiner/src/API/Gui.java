package API;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

/**
 * Created by Peter on 6/24/2017.
 */
public class Gui extends JFrame {

    private JPanel contentPane;

    public Gui(API script) {
        setTitle("SMiner");
        setResizable(false);
        setType(Type.UTILITY);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 90, 52);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(null);
        setContentPane(contentPane);

        JButton btnPlay = new JButton("Play unfair!");
        btnPlay.addActionListener(e -> {
            if(script.getRocks().size() > 0) {
                setVisible(false);
            }
        });

        btnPlay.setBounds(0, 0, 130, 23);
        contentPane.add(btnPlay);

        setVisible(true);
    }

}
