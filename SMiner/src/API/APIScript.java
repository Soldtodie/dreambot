package API;

import org.dreambot.api.methods.MethodContext;

/**
 * Created by Peter on 6/20/2017.
 */
public class APIScript {

    private API script;

    public APIScript(API script) {
        this.script = script;
    }

    protected MethodContext getScript() {
        return script.getScript();
    }

    protected API getDoc() {
        return script;
    }

}
