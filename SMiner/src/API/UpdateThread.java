package API;

/**
 * Created by Peter on 7/13/2017.
 */
public class UpdateThread extends APIScript implements Runnable {

    public UpdateThread(API script) {
        super(script);
    }

    @Override
    public void run() {
        while(getScript().getClient().getInstance().getScriptManager().getCurrentScript() != null && getScript().getClient().isLoggedIn()) {
            getDoc().getRocks().forEach(e -> {
                e.update();
                e.updateCosts(e.getScript().getLocalPlayer());
            });
            try {
                Thread.sleep(300);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        }
    }
}
