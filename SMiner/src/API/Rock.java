package API;

import org.dreambot.api.methods.MethodContext;
import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.wrappers.interactive.Entity;
import org.dreambot.api.wrappers.interactive.GameObject;
import org.dreambot.api.wrappers.interactive.Player;

import java.awt.*;
import java.awt.geom.Area;

/**
 * Created by Peter on 6/20/2017.
 */
public class Rock extends APIScript {

    private static Rock currentRock = null;
    private static Rock hoverRock = null;

    private String name = "Mined";
    private long spawnTime = -1;
    private int timeTillRespawn = -1;
    private boolean available = false;
    private Tile position;
    private GameObject obj;
    private AverageList averageMineTimeList = new AverageList(20);
    private long averageMineTime = -1;
    private int costs = 0;

    public Rock(API script, Tile position) {
        super(script);
        this.position = position;
        refresh();
        if (obj != null) {
            set();
        }
    }

    public Rock(API script, GameObject obj) {
        super(script);
        this.obj = obj;
        this.position = obj.getTile();
        set();
    }

    public Rock(API script, Rock rock) {
        super(script);

        this.timeTillRespawn = rock.timeTillRespawn;
        this.averageMineTimeList = rock.averageMineTimeList;
        this.averageMineTime = rock.averageMineTime;
        this.obj = rock.obj;
        this.position = rock.getPosition();

        refresh();
        if (obj != null) {
            set();
        }

    }

    public void refresh() {
        if(getScript().getMap().isLocal(position)) {
            obj = getScript().getGameObjects().getTopObjectOnTile(position);
            if(obj != null && name.equals("Mined")) {
                set();
            }
        }
    }

    public int getCosts() {
        return this.costs;
    }

    public void updateCosts(Player player) {

        // spawnTime, walkTime, turnTime

        int costs = 0;

        costs += getDoc().getUtility().getTimeForFacing(player, getPosition(), false);

        if(isAvailable()) {

        } else {
            costs += getCalculatedSpawnTime();
        }
        costs += (getPosition().distance(getScript().getLocalPlayer().getTile())-1) * 512;
        this.costs = costs;
    }

    public int getTimeTillRespawn() {
        return timeTillRespawn;
    }

    public GameObject getObject() {
        return this.obj;
    }

    public String getName() {
        return this.name;
    }

    public static Rock getCurrentRock() {
        return currentRock;
    }

    public static void setCurrentRock(Rock rock) {
        currentRock = rock;
    }

    public static Rock getHoverRock() {
        return hoverRock;
    }

    public static void setHoverRock(Rock rock) {
        hoverRock = rock;
    }

    public boolean isAvailable() {
        return available;
    }

    public void update() {
        isMined();
    }

    public boolean isMined() {

        refresh();

        available = !(obj == null || obj.getModelColors() == null);

        setSpawnTime();

        return !available;
    }

    public void setSpawnTime() {
        if(available) {
            if(timeTillRespawn > 99999 || timeTillRespawn == -1) {
                timeTillRespawn = (int) (System.currentTimeMillis() - spawnTime);
            }
            spawnTime = -1;
        } else {
            if(spawnTime == -1 && timeTillRespawn > 0) {
                spawnTime = System.currentTimeMillis();
            }
        }
    }

    public long getSpawnTime() {
        return spawnTime;
    }

    public int getCalculatedSpawnTime() {
        if(getSpawnTime() == -1) {
            return 0;
        }
        return (int)(System.currentTimeMillis() - getSpawnTime());
    }

    private void set() {
        Rocks rock = Rocks.get(obj.getModelColors());
        if(rock != null) {
            this.name = rock.getName();
        }
    }

    public Tile getPosition() {
        return this.position;
    }

    public boolean isFacing(Player player) {
        return getDoc().getUtility().isFacing(player, getPosition(), true);
    }

    public static Rock getRockUnderMouse(API script) {
        for(Entity ent : script.getScript().getMouse().getEntitiesOnCursor()) {
            if(ent != null && ent.getName() != null && ent.getName().equals("Rocks")) {
                return new Rock(script, (GameObject)ent);
            }
        }
        return null;
    }

    public void draw(Graphics2D graphics, Color color, Color nameColor, boolean removeButton) {
        if(obj != null && obj.isOnScreen()) {
            graphics.setColor(color);
            Area area = getScript().getClient().getViewportTools().getModelArea(obj);
            if(area != null) {
                graphics.draw(area);
                if(removeButton) {
                    Point p = getButtonPosition();
                    if (p != null) {
                        //graphics.setColor(nameColor);
                        //graphics.drawString("" + name == null ? "Null" : name, p.x, p.y);
                        RemoveButton.draw(graphics, p, getScript().getMouse().getPosition());
                    }
                }
            }
        }
    }

    public boolean interact() {

        if(!obj.isOnScreen()) {
            getScript().getCamera().rotateToEntity(obj);

            //SLEEP

        }

        return obj.interact("Mine");
    }

    public boolean hover() {

        if(!obj.isOnScreen()) {
            getScript().getCamera().rotateToEntity(obj);

            //SLEEP

        }

        return obj.interact("");
    }

    public boolean isHovering() {

        Area are = getScript().getClient().getViewportTools().getModelArea(obj);

        if(are != null) {
            return are.contains(getScript().getMouse().getPosition());
        }

        return false;
    }

    public Point getButtonPosition() {
        Point p = obj.getCenterPoint();
        if(p != null) {
            p.translate(-12, -12);
            return p;
        }
        return null;
    }

    @Override
    public boolean equals(Object o) {
        return ((Rock)o).getPosition().equals(getPosition());
    }
}
