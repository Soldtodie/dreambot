package API;

import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.wrappers.interactive.Player;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

/**
 * Created by Peter on 6/21/2017.
 */
public class Utility extends APIScript {

    public Utility(API script) {
        super(script);
    }

    //Only for distance == 1
    public int getRotationForFacing(Player player, Tile pos, boolean onlyCardinal) {

        if(player != null && pos != null) {
            for(int i = 0; i < (onlyCardinal ? 4 : 8); i++) {
                if(player.getTile().translate(Rotation.values()[i].getDirection()).equals(pos)) {
                    return Rotation.values()[i].getRotation();
                }
            }
        }

        return -1;
    }

    public int getTimeForFacing(Player player, Tile pos, boolean onlyCardinal) {

            int rot = getRotationForFacing(player, pos, onlyCardinal);

            if (rot != -1) {
                int rotation = Math.abs(rot - Math.abs(player.getRotation()));
                if (rotation > Rotation.NORTH.getRotation()) {
                    rotation = 2048 - rotation;
                }
                return rotation;
            }

        return 0;
    }

    public Tile getTilePlayerIsFacing(Player player, boolean onlyCardinal) {

        if(player != null) {
            Rotation rot = getRotation(player.getRotation(), onlyCardinal ? 4 : 8);
            if(rot != null) {
                return player.getTile().translate(rot.getDirection());
            }
        }

        return null;
    }

    public Rotation getRotation(int rotation, int loop) {
        for(int i = 0; i < loop; i++) {
            if(Math.abs(Rotation.values()[i].getRotation() - rotation) <= 128) {
                return Rotation.values()[i];
            }
        }
        return null;
    }

    public boolean isFacing(Player player, Tile pos, boolean onlyCardinal) {

        if(player != null && pos != null) {
            Rotation rot = getRotation(player.getRotation(), onlyCardinal ? 4 : 8);
            if(rot != null) {
                if(player.getTile().translate(rot.getDirection()).equals(pos)) {
                    return true;
                }
            }
        }

        return false;
    }

    public static BufferedImage getImage(String url) {
        try {
            return ImageIO.read(new URL(url));
        } catch(IOException e) {
            return null;
        }
    }

    public static String getElapsedTimeHoursMinutesSecondsString(long elapsedTime) {
        String format = String.format("%%0%dd", 2);
        elapsedTime = elapsedTime / 1000;
        String seconds = String.format(format, elapsedTime % 60);
        String minutes = String.format(format, (elapsedTime % 3600) / 60);
        String hours = String.format(format, elapsedTime / 3600);
        String time =  hours + ":" + minutes + ":" + seconds;
        return time;
    }

}
