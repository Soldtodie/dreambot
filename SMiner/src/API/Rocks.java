package API;

/**
 * Created by Peter on 27.05.2016.
 */
public enum Rocks {

    MINED("Mined", -1),
    CLAY("Clay", 6705),
    COPPER("Copper", 4645),
    TIN("Tin", 53),
    IRON("Iron", 2576),
    SILVER("Silver", 74),
    COAL("Coal", 10508),
    GOLD("Gold", 8885),
    MITHRIL("Mithril", -22239),
    ADAMANTITE("Adamantite", 21662),
    RUNITE("Runite", -31437);

    final String name;
    final short color;

    public static Rocks get(short colors[]) {

        if(colors != null) {
            for (Rocks col : Rocks.values()) {
                if (col.color == colors[0]) {
                    return col;
                }
            }
        }

        return Rocks.MINED;
    }

    public String getName() {
        return name;
    }

    Rocks(String name, int color) {
        this.name = name;
        this.color = (short) color;
    }

}