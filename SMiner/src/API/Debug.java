package API;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by Peter on 7/11/2017.
 */
public class Debug {

    public static void draw(Graphics2D g, Point location, Color backgroundColor, String... text) {

        int size = 0;
        for(String str : text) {
            int nSize = g.getFontMetrics().stringWidth(str);
            size = nSize > size ? nSize : size;
        }

        g.setColor(new Color(backgroundColor.getRed(), backgroundColor.getBlue(), backgroundColor.getGreen(), 120));
        g.fillRect(location.x, location.y, size + 4, g.getFontMetrics().getHeight() * text.length + 2 * text.length + 2);

        g.setColor(Color.WHITE);

        int i = 0;
        for(String str : text) {
            g.drawString(str, location.x + 2, location.y + g.getFontMetrics().getHeight() + i * g.getFontMetrics().getHeight());
            i++;
        }

    }

}
