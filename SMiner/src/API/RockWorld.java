package API;

import java.util.ArrayList;

/**
 * Created by Peter on 6/22/2017.
 */
public class RockWorld {
    private int world;
    private ArrayList<Rock> rocks = new ArrayList<>();

    public RockWorld(int world) {
        this.world = world;
        rockWorlds.add(this);
    }

    public int getWorld() {
        return this.world;
    }

    public ArrayList<Rock> getRocks() {
        return this.rocks;
    }

    private static ArrayList<RockWorld> rockWorlds = new ArrayList<>();

    public static RockWorld getRockWorld(int world) {
        for(RockWorld rw : rockWorlds) {
            if(rw.getWorld() == world) {
                return rw;
            }
        }
        addWorld(world);
        return getRockWorlds().get(getRockWorlds().size()-1);
    }

    public static ArrayList<Rock> getRocks(int world) {
        return getRockWorld(world).getRocks();
    }

    public static ArrayList<RockWorld> getRockWorlds() {
        return rockWorlds;
    }

    public static void addWorld(int world) {
        RockWorld.getRockWorlds().add(new RockWorld(world));
    }

    public static void addRockToWorld(API script, int world, Rock rock) {
        getRocks(world).add(new Rock(script, rock));
    }

}
