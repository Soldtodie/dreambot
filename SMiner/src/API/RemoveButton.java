package API;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by Peter on 6/22/2017.
 */
public class RemoveButton {

    private static BufferedImage img_standard;
    private static BufferedImage img_hover;
    private static boolean hoverEnabled = false;

    public RemoveButton() {

    }

    public static boolean isOver(Point position, Point mouse_pos) {
        if(position == null || mouse_pos == null || img_standard == null) return false;
        return new Rectangle(position.x, position.y, img_standard.getWidth(), img_standard.getHeight()).contains(mouse_pos);
    }

    public static void setImg_standard(String url) {
        img_standard = Utility.getImage(url);
    }

    public static void setImg_hover(String url) {
        img_hover = Utility.getImage(url);
    }

    public static void draw(Graphics2D graphics, Point position, Point mouse_pos) {
        if(hoverEnabled && isOver(position, mouse_pos)) {
            graphics.drawImage(img_hover, position.x, position.y, null);
        } else {
            graphics.drawImage(img_standard, position.x, position.y, null);
        }
    }

    public static void setHoverEnabled(boolean enable) {
        hoverEnabled = enable;
    }

    public static boolean isHoverEnabled() {
        return hoverEnabled;
    }

}
