package API;

import org.dreambot.api.wrappers.items.Item;

import java.util.Arrays;

/**
 * Created by Peter on 6/20/2017.
 */
public class Inventory extends APIScript {

    public Inventory(API script) {
        super(script);
    }

    public boolean dropAllExcept(DropStyle style, String... items) {
        if(items != null) {
            for(int index : style.get()) {
                Item it = getScript().getInventory().getItemInSlot(index);
                if(it != null) {
                    if (!Arrays.asList(items).stream().anyMatch(e -> it.getName().toLowerCase().contains(e))) {

                        it.interact("Drop");

                        //SLEEP

                        getScript().sleep(50, 200);

                    }
                }
            }

        }

        return false;
    }

}
