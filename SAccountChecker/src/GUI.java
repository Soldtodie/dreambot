/**
 * Created by Peter on 7/21/2017.
 */
import org.dreambot.api.script.AbstractScript;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;

import java.awt.Cursor;

public class GUI extends JFrame {

    private JPanel contentPane;
    private JTextField tfSeperator;
    private JLabel labelWPM;

    public GUI(SAccountChecker script) {
        setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        setResizable(false);
        setTitle("SAccountChecker");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 450, 220);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        try { UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName()); } catch(Exception e) {}

        JButton btnSelectLoadFile = new JButton("Select account list");
        btnSelectLoadFile.addActionListener((e) -> {

            JFileChooser chooser = new JFileChooser();
            chooser.setCurrentDirectory(new java.io.File(System.getProperty("scripts.path")));
            chooser.setDialogTitle("Select save directory");
            chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            chooser.setAcceptAllFileFilterUsed(false);

            if(chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {

                btnSelectLoadFile.setText(chooser.getSelectedFile().toString());
                script.file = chooser.getSelectedFile().toString();

            }

        });
        btnSelectLoadFile.setBounds(10, 11, 414, 23);
        contentPane.add(btnSelectLoadFile);

        JButton btnSelectSaveFile = new JButton("Select output folder");
        btnSelectSaveFile.addActionListener((e) -> {

            JFileChooser chooser = new JFileChooser();
            chooser.setCurrentDirectory(new java.io.File(System.getProperty("scripts.path")));
            chooser.setDialogTitle("Select save directory");
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            chooser.setAcceptAllFileFilterUsed(false);

            if(chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {

                btnSelectSaveFile.setText(chooser.getSelectedFile().toString());
                script.checkedFile = chooser.getSelectedFile().toString();

            }

        });
        btnSelectSaveFile.setBounds(10, 45, 414, 23);
        contentPane.add(btnSelectSaveFile);

        JLabel lblSeperator = new JLabel("Seperator:");
        lblSeperator.setBounds(10, 79, 60, 14);
        contentPane.add(lblSeperator);

        tfSeperator = new JTextField();
        tfSeperator.setToolTipText("Standard: username:password");
        tfSeperator.setHorizontalAlignment(SwingConstants.CENTER);
        tfSeperator.setText(":");
        tfSeperator.setBounds(80, 76, 344, 20);
        contentPane.add(tfSeperator);
        tfSeperator.setColumns(10);

        labelWPM = new JLabel();
        labelWPM.setBounds(321, 132, 46, 14);
        contentPane.add(labelWPM);

        JSlider sliderWPM = new JSlider();
        sliderWPM.addChangeListener((e) -> labelWPM.setText(sliderWPM.getValue() + ""));
        sliderWPM.setMaximum(350);
        sliderWPM.setMinimum(50);
        sliderWPM.setValue(200);
        sliderWPM.setBounds(230, 157, 194, 26);
        contentPane.add(sliderWPM);

        JCheckBox chckbxPaint = new JCheckBox("Paint?");
        chckbxPaint.setSelected(true);
        chckbxPaint.setBounds(6, 117, 97, 23);
        contentPane.add(chckbxPaint);

        JLabel lblWordsPerMinute = new JLabel("Words per minute:");
        lblWordsPerMinute.setBounds(288, 107, 97, 14);
        contentPane.add(lblWordsPerMinute);

        JButton btnStart = new JButton("Start");
        btnStart.addActionListener((e) -> {

            script.wpm = sliderWPM.getValue();
            script.paint = chckbxPaint.isSelected();
            script.seperator = tfSeperator.getText();

            setVisible(false);

        });
        btnStart.setBounds(10, 157, 194, 23);
        contentPane.add(btnStart);
    }
}

