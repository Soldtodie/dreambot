/**
 * Created by Peter on 7/20/2017.
 */

import org.dreambot.api.methods.tabs.Tab;
import org.dreambot.api.randoms.RandomEvent;
import org.dreambot.api.script.AbstractScript;
import org.dreambot.api.script.Category;
import org.dreambot.api.script.ScriptManifest;
import org.dreambot.api.wrappers.widgets.WidgetChild;
import org.dreambot.server.net.datatype.LoginResponse;

import java.awt.*;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@ScriptManifest(author = "Soldtodie", category = Category.MISC, description = "Check accounts", image = "", name = "SAccountChecker", version = 1.1)

public class SAccountChecker extends AbstractScript {

    GUI gui;

    LoginHandler handler;
    ArrayList<Account> accounts = new ArrayList<>();
    String file;
    String checkedFile;
    String seperator;
    boolean paint;
    int wpm;
    int index = 1;
    int available = 0;
    int banned = 0;
    int locked = 0;
    int invalid = 0;
    int tested = 0;

    @Override
    public void onStart() {

        //getRandomManager().disableSolver(RandomEvent.WELCOME_SCREEN);

        gui = new GUI(this);
        gui.setVisible(true);

        while(gui.isVisible()) {
            sleep(250);
        }

        gui.dispose();

        if(file == null || checkedFile == null || seperator == null || seperator.equals("")) {
            stop();
            return;
        }

        try(Stream<String> stream = Files.lines(Paths.get(file))) {

            stream.forEach(s -> {

                if(s != null && !s.equals("") && s.contains(seperator)) {

                    String[] data = s.split(seperator);

                    accounts.add(new Account(data[0], data[1]));

                }

            });

        } catch(IOException e) {
            log(e.getMessage());
        }

        if(accounts.size() == 0) {
            log("No accounts found!");
            stop();
            return;
        }

        account = accounts.get(0);
        handler = new LoginHandler(getClient().getMethodContext(), account, wpm);

        if(handler.isInvalidScreen()) {
            handler.clickButton(handler.tryAgain);
        }
        else if (handler.isCredentialScreen()) {
            handler.clickButton(handler.cancel);
        }

    }

    Account account;

    @Override
    public int onLoop() {

        if(handler.hasFinished()) {
            //log(handler.getUsername() + " : " + handler.response);
            account.setResponse(handler.response);

            tested++;

            if(handler.response.equals("DISABLED")) {
                banned++;
            }
            else if(handler.response.equals("LOCKED")) {
                locked++;
            }
            else if(handler.response.equals("AVAILABLE")) {
                available++;
            }
            else if(handler.response.equals("INVALID")) {
                invalid++;
            }

            if(index >= accounts.size()) {
                stop();
                return 100;
            } else {
                //log("Next account!");
                account = accounts.get(index++);
                handler.switchAccount(account);
            }
        }

        if(handler.login()) {

            if(getTabs().isOpen(Tab.LOGOUT) || getTabs().open(Tab.LOGOUT)) {
                sleep(50, 360);
                List<WidgetChild> wg = getWidgets().getWidgets(widgetChild -> widgetChild.getText() != null && widgetChild.getActions() != null && widgetChild.getActions()[0].equals("Logout"));

                if(wg != null && wg.size() > 0) {
                    wg.get(0).interact("Logout");
                    sleepUntil(() -> !getClient().isLoggedIn(), 3500);
                }

            }

        }

        return 250;
    }

    @Override
    public void onPaint(Graphics graphics) {
        if(paint) {
            graphics.setColor(Color.YELLOW);
            graphics.drawString("Current account(" + (tested+1) + "/" + accounts.size() + "): " + account.getUsername(), 10, 50);
            graphics.drawString("Available: " + available, 10, 65);
            graphics.drawString("Banned: " + banned, 10, 80);
            graphics.drawString("Locked: " + locked, 10, 95);
            graphics.drawString("Invalid: " + invalid, 10, 110);
        }
    }

    @Override
    public void onExit() {

        log(tested + " accounts checked!");
        log("Available: " + available);
        log("Banned: " + banned);
        log("Locked: " + locked);
        log("Invalid: " + invalid);

        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(checkedFile + "\\checked.txt")))
        {
            accounts.forEach(e -> {
                try {
                    writer.write(e.getUsername() + seperator + e.getPassword() + seperator + e.getResponse());
                    writer.newLine();
                } catch (IOException e1) {
                    log(e1.getMessage());
                }
            });
        } catch (IOException e) {
            log(e.getMessage());
        }

        if(gui != null) {
            gui.setVisible(false);
            gui.dispose();
        }
        /*
        accounts.clear();
        accounts = null;
        account = null;
        handler = null;
        index = 1;
        */
    }
}
