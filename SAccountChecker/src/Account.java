/**
 * Created by Peter on 7/20/2017.
 */
public class Account {

    private String username;
    private String password;
    private String response = "NOT-TESTED";

    public Account(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }
}
