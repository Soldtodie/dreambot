import org.dreambot.api.data.GameState;
import org.dreambot.api.data.LoginStatus;
import org.dreambot.api.methods.MethodContext;
import org.dreambot.api.methods.RSLoginResponse;
import org.dreambot.api.methods.widget.Widget;
import org.dreambot.api.wrappers.widgets.WidgetChild;
import org.dreambot.server.net.datatype.LoginResponse;

import java.awt.*;

/**
 * Created by Peter on 7/20/2017.
 */

public class LoginHandler {

    private MethodContext script;
    private String username;
    private String password;

    final Point credentialLogin = new Point(313, 250);
    final Point credentialPassword = new Point(347, 261);
    final int credentailCheckLength = 10;

    final Point tryAgain = new Point(314, 259);
    final Point cancel = new Point(393, 304);
    final Point login = new Point(234, 304);
    final Point existingUser = new Point(393, 276);

    final Point buttonSize = new Point(130, 30);

    String response;

    public LoginHandler(MethodContext script, String username, String password, int wpm) {
        this.script = script;
        switchAccount(username, password);
        script.getMouse().getMouseSettings().setWordsPerMinute(wpm);
    }

    public LoginHandler(MethodContext script, Account account, int wpm) {
        this.script = script;
        switchAccount(account);
        script.getMouse().getMouseSettings().setWordsPerMinute(wpm);
    }

    public String getUsername() {
        return username;
    }

    public void switchAccount(Account account) {
        this.username = account.getUsername();
        this.password = account.getPassword();
        this.response = "";
    }

    public void switchAccount(String username, String password) {
        this.username = username;
        this.password = password;
        this.response = "";
    }

    public boolean hasFinished() {
        return response != "" && !isLoggedIn();
    }

    public boolean login() {

        if(isStartScreen()) {
            //Click Button "Existing User"
            clickButton(existingUser);
        } /*else if(isLoggedIn() && isWelcomeScreen()) {
            WidgetChild wg = getWelcomeScreenButton();
            if(wg != null) {
                wg.interact();
            }

        } */else if(isInvalidScreen()) {
            //Click Button "Try again"
            response = "INVALID";
            clickButton(tryAgain);
        } else {
            if(script.getClient().getLoginResponse().equals(RSLoginResponse.DISABLED)) {
                response = "DISABLED";
            } else if(script.getClient().getLoginResponse().equals(RSLoginResponse.ACCOUNT_LOCKED)) {
                response = "LOCKED";
            } else if(script.getClient().getLoginResponse().equals(RSLoginResponse.MEMBERS_WORLD)) {
                script.log("Select a non-member world!");
                script.getClient().getInstance().getScriptManager().getCurrentScript().stop();
            } else if(script.getClient().getLoginResponse().equals(RSLoginResponse.ADDRESS_BLOCKED) || script.getClient().getLoginResponse().equals(RSLoginResponse.SERVER_UPDATED) || script.getClient().getLoginResponse().equals(RSLoginResponse.UPDATED)) {
                script.getClient().getInstance().getScriptManager().getCurrentScript().stop();
            } else if(!script.getClient().getLoginResponse().equals(RSLoginResponse.CONNECTING_TO_SERVER)) {
                //Click Button "Cancel"
                clickButton(cancel);
            }
            if ((!isLoading() || !isLoggingIn()) && isCredentialScreen()) {
                if (isCredentialEmpty()) {
                    script.getKeyboard().type(username, true);
                    script.sleep(150, 560);
                    script.getKeyboard().type(password, true);
                    script.sleep(500);
                } else {
                    //Click Button "Cancel"
                    clickButton(cancel);
                }
            }
        }

        if(isLoggedIn()) {
            response = "AVAILABLE";
            return true;
        }
        return false;
    }

    public boolean clickButton(Point location) {
        return script.getMouse().click(new Rectangle(location.x, location.y, buttonSize.x, buttonSize.y));
    }

    public void get() {
        script.log("" + script.getClient().getGameState());
        script.log("" + script.getClient().getLoginIndex());
        script.log("" + script.getClient().getLoginResponse().name());
        script.log("" + script.getClient().getLoginResponse().getSeverity());
        script.log("" + isLoginInformationEmpty());
        script.log("" + isPasswordInformationEmpty());
        script.log("" + script.getClient().getLoginResponse().getSeverity());
        script.log("");
    }

    public WidgetChild getWelcomeScreenButton() {
        Widget w = script.getWidgets().getWidget(378);
        if(w != null) {
            WidgetChild wg = w.getChild(6);
            if(wg != null) {
                return wg;
            }
        }
        return null;
    }

    public boolean isWelcomeScreen() {
        WidgetChild wg = getWelcomeScreenButton();
        if(wg != null) {
            return wg.isVisible();
        }
        return false;
    }

    public boolean isStartScreen() {
        return script.getClient().getLoginIndex() == 0 && script.getClient().getGameState().equals(GameState.LOGIN_SCREEN);
    }

    public boolean isCredentialScreen() {
        return script.getClient().getLoginIndex() == 2 && script.getClient().getGameState().equals(GameState.LOGIN_SCREEN);
    }

    public boolean isLoggingIn() {
        return script.getClient().getLoginIndex() == 2 && script.getClient().getGameState().equals(GameState.LOGGING_IN);
    }

    public boolean isInvalidScreen() {
        return script.getClient().getLoginIndex() == 3 && script.getClient().getGameState().equals(GameState.LOGIN_SCREEN);
    }

    public boolean isLoading() {
        return script.getClient().getLoginIndex() == 2 && script.getClient().getGameState().equals(GameState.LOADING);
    }

    public boolean isLoggedIn() {
        return script.getClient().getLoginIndex() == 2 && script.getClient().getGameState().equals(GameState.LOGGED_IN);
    }

    public boolean isLoginInformationEmpty() {
        for(int i = 0; i < credentailCheckLength; i++) {
            if(script.getColorPicker().getColorAt(credentialLogin.x + i, credentialLogin.y).equals(Color.WHITE)) {
                return false;
            }
        }
        return true;
    }

    public boolean isPasswordInformationEmpty() {
        for(int i = 0; i < credentailCheckLength; i++) {
            if(script.getColorPicker().getColorAt(credentialPassword.x + i, credentialPassword.y).equals(Color.WHITE)) {
                return false;
            }
        }
        return true;
    }

    public boolean isCredentialEmpty() {
        return isLoginInformationEmpty() && isPasswordInformationEmpty();
    }

}
